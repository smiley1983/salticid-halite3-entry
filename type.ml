(* OCaml Starter for Halite III on Halite.io
   This code is public domain. There is no warranty.
 *)

type direction = 
  | North 
  | East 
  | South 
  | West 
  | Still 
;;

type command = 
  | Generate 
  | Construct of int
  | Move of (int * direction)
;;

type entity_type = | Ship | Shipyard | Dropoff;;

(* the halite field is only used for ships, id field is not for shipyards *)
type entity = 
 {
  entity_type : entity_type;
  id : int;
  owner : int;
  mutable position : int * int; (* row, col *)
  mutable halite : int;
 }
;;

(* This is the data structure I originally used for entities. It seemed neat
 * at first and it doesn't waste as much memory for Shipyards or Dropoffs, 
 * but it requires pattern matching every time you want to access the 
 * entity_data. Wasting a bit of memory is preferable in this case - there
 * are very few shipyards or dropoffs, and lots of places in the code where
 * you want to access the entity data.
type entity_data = 
 {
  mutable id : int;
  mutable owner : int;
  mutable position : int * int; (* row, col *)
 }
;;
type entity =
  | Dropoff of entity_data
  | Shipyard of entity_data
  | Ship of (entity_data * int) (* the int is the halite_amount *)
;;
*)

type constant_set = {
  mutable capture_enabled : bool;
  mutable capture_radius : int;
  mutable default_map_height : int;
  mutable default_map_width : int;
  mutable dropoff_cost : int;
  mutable dropoff_penalty_ratio : int;
  mutable extract_ratio : int;
  mutable factor_exp_1 : float;
  mutable factor_exp_2 : float;
  mutable initial_energy : int;
  mutable inspiration_enabled : bool;
  mutable inspiration_radius : int;
  mutable inspiration_ship_count : int;
  mutable inspired_bonus_multiplier : float;
  mutable inspired_extract_ratio : int;
  mutable inspired_move_cost_ratio : int;
  mutable max_cell_production : int;
  mutable max_energy : int;
  mutable max_players : int;
  mutable max_turns : int;
  mutable max_turn_threshold : int;
  mutable min_cell_production : int;
  mutable min_turns : int;
  mutable min_turn_threshold : int;
  mutable move_cost_ratio : int;
  mutable new_entity_energy_cost : int;
  mutable persistence : float;
  mutable ships_above_for_capture : int;
  mutable strict_errors : bool;
  mutable game_seed : int;
};;

type assignment = 
  | Collect
  | Return
  | Construct
  | Blockade
  | Sabotage
;;

type sorted_search_cell =
 {
  visited : bool;
  score : float;
  turns_here : int;
  turns_total : int;
  profit_here : int;
  profit_total : int;
  cell_row : int;
  cell_col : int;
  predecessor : sorted_search_cell option;
 }
;;

type collection_cycle =
 {
  mutable turn_begun : int;
  mutable turn_completed : int;
  mutable amount_returned : int;
 }
;;

type p_ship =
 {
  mutable entity : entity;
  mutable desired_moves : desire list;
  mutable prev_position : int * int;
  mutable move_decided : bool;
  mutable assignment : assignment;
  mutable medium_destination : (int * int) option;
  mutable target : target option;
  mutable plan : plan option;
  mutable possible_moves : plan list;
  mutable inspiration_relevant : bool;
  mutable could_inspire : p_ship list;
  mutable last_collection : collection_cycle option;
  mutable current_collection : collection_cycle option;
  mutable turns_stationary : int;
  mutable crash_risk_wait : int;
  mutable in_danger : bool;
  mutable chased : bool;
  mutable chaser : bool;
  mutable contested : (int * int) list;
  mutable contested_target : (int * int) option;
 }
and desire = {
  p_ship : p_ship;
  command : command;
  destination : int * int;
  mutable weight : float;
}
and plan = {
  plan_direction : direction;
  plan_position : int * int;
  mining : bool;
  mutable happiness : float;
  plan_ship : p_ship;
}
and target =
  | Base of entity * ((int * int) list)
  | Harvest of p_ship * ((int * int) list)
  | Crash_site of (int * int)
;;

type magic_value =
  | Fraction of int * int
  | Integer of int
  | Float of float
;;

type magic_set = {
  advisable_dropoff_quality_ratio : magic_value;
  advisable_dropoff_continuation_ratio : magic_value;
  ship_build_payoff_mul : magic_value;
  dangerous_load_mul : magic_value;
  enemy_dropoff_exclusion_radius : int;
  friendly_dropoff_exclusion_radius : int;
  endgame_high_halite : int;
  crash_warn_downplay : float;
  min_ships_before_dropoff: int;
};;

type mybot_state = { 
  mutable p_ships : p_ship list;
  mutable desire_map : desire list array array;
  mutable intent_map : desire list array array;
  mutable condense_factor : int;
  mutable condensed_map : int array array;
  mutable inspired_condensed_map : int array array;
  mutable condensed_area_under_threat : bool array array;
  mutable under_threat : bool array array;
  mutable searches : int array array array array;
  mutable dropoff_search : int array array;
  mutable return_search : int array array;
  mutable safe_dropoff_search : int array array;
  mutable crash_search : int array array;
  mutable weighted_dropoff_search : int array array;
  mutable all_dropoff_searches : int array array array;
  mutable all_enemy_search : int array array;
  mutable obstacles: bool array array;
  mutable obstacles_updated: bool;
  mutable ships_near : p_ship list array array;
  mutable half_sum_threshold : int;
  mutable percentile_thresholds : int list;
  mutable next_dropoff_positions : (int * int) list;
  mutable constructed_this_turn : int;
  mutable construction_decided : bool;
  mutable targets : target list;
  mutable magic : magic_set;
  mutable stupidity : float;
  mutable total_halite_available : int;
  mutable save_for_dropoff : bool;
  mutable crash_risk_occupied : (int * int) array array; (* times did risk * times didn't risk by turns by players *)
  mutable crash_risk_unoccupied : (int * int) array array; (* times did risk * times didn't risk by turns by players *)
  (*mutable crash_risk_retreat_halite : int list array; (* halite amount by data points by players*)*)
  mutable crash_risk_attack_halite : int list array; (* halite amount by data points by players*)
  mutable danger_maps : int array array array;
  mutable risk_turns : int;
};;

type game_state = {
  mutable tokens : string list;
  mutable num_players : int;
  mutable my_id : int;
  mutable width : int;
  mutable height: int;
  mutable map : int array array;
  mutable prev_map : int array array;
  mutable ships : entity list;
  mutable shipyards : entity list;
  mutable dropoffs : entity list;
  mutable energy : int array;
  mutable turn : int;
  mutable last_update : float;
  mutable persist : mybot_state;
  const : constant_set;
};;

type f_move_data = {
  f_id : int;
  f_dir : direction;
  f_pos : (int * int);
  f_collect : int;
};;

type f_command = 
  | FConstruct of int
  | FMove of f_move_data
;;

type f_ship = {
  f_entity : entity;
  f_commands : f_command option array;
  exist_from : int;
}

type future_search = {
  mutable halite : int array array array;
  mutable occupied : bool array array array;
  mutable visited : bool array array;
  mutable generate : bool array;
  mutable f_ships : f_ship list;
  mutable future_turns : int;
};;

(*
type sim_ship =
 {
  mutable sim_entity : entity;
  mutable exists_from : int;
  mutable future : (int * int) list;
 }
;;

type sim_state = {
  mutable real : game_state;
  mutable sim_map : int array array;
  mutable sim_ships : entity list;
  mutable sim_shipyards : entity list;
  mutable sim_dropoffs : entity list;
  mutable sim_energy : int array;
}
*)

