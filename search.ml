
let get_adjacent grid loc =
  let size = (Array.length grid, Array.length grid.(0)) in
  List.map (fun offset -> 
    Game.wrap_grid_position size (Game.add_position loc offset)
  ) Game.list_move_offsets
;;

let simple_bfs_visit dist loc next q =
  let p_row, p_col = loc in
  List.iter (fun (n_row, n_col) ->
    if dist.(n_row).(n_col) = max_int then (
      dist.(n_row).(n_col) <- dist.(p_row).(p_col) + 1;
      Queue.push (n_row, n_col) q;
    )
  ) next
;;

let avoid_visit dist avoid loc next q =
  let p_row, p_col = loc in
  List.iter (fun (n_row, n_col) ->
    if dist.(n_row).(n_col) = max_int && (not (List.exists (fun (tr, tc) -> tr = n_row && (tc = n_col)) avoid)) then (
      dist.(n_row).(n_col) <- dist.(p_row).(p_col) + 1;
      Queue.push (n_row, n_col) q;
    )
  ) next
;;

let voronoi_visit dist owned loc next q =
  let p_row, p_col = loc in
  List.iter (fun (n_row, n_col) ->
    if dist.(n_row).(n_col) = max_int then (
      dist.(n_row).(n_col) <- dist.(p_row).(p_col) + 1;
      owned.(n_row).(n_col) <- owned.(p_row).(p_col);
      Queue.push (n_row, n_col) q;
    )
  ) next
;;

let dist_visit_adjacent dist (p_row, p_col) (n_row, n_col) q =
  if dist.(n_row).(n_col) = max_int then (
    dist.(n_row).(n_col) <- dist.(p_row).(p_col) + 1;
    Queue.push (n_row, n_col) q;
  )
;;

let sum_dist_visit_adjacent source dist max_dist min_add total (p_row, p_col) (n_row, n_col) q =
  if (dist.(p_row).(p_col) < max_dist) 
  && (dist.(n_row).(n_col) = max_int) then (
    dist.(n_row).(n_col) <- dist.(p_row).(p_col) + 1;
    if (source.(n_row).(n_col) >= min_add) then (
      total := !total + source.(n_row).(n_col);
    );
    Queue.push (n_row, n_col) q;
  )
;;

let configurable_visit visit_adjacent loc next q =
  List.iter (fun next_loc ->
    visit_adjacent loc next_loc q
  ) next
;;

let sum_dist_visit source dist max_dist min_add ref_total =
  let visit_adjacent = sum_dist_visit_adjacent source dist max_dist min_add ref_total in
  configurable_visit visit_adjacent
;;

(* FIXME return to this later
let count_halite_opponent_side_visit state total dist_limit dist owner loc next q =
  let p_row, p_col = loc in
  List.iter (fun (n_row, n_col) ->
    if dist.(n_row).(n_col) = max_int 
    && owner.(n_row).(n_col) != state.my_id 
    && dist.(p_row).(p_col) <= dist_limit then (
      dist.(n_row).(n_col) <- dist.(p_row).(p_col) + 1;
      total := !total + state.map.(n_row).(n_col);
      Queue.push (n_row, n_col) q;
    )
  ) next
;;
*)

let mark_bounds_visit bounds cond dist loc next q =
  let p_row, p_col = loc in
  List.iter (fun (n_row, n_col) ->
    if dist.(n_row).(n_col) = max_int then (
      dist.(n_row).(n_col) <- dist.(p_row).(p_col) + 1;
      if cond loc (n_row, n_col) then (
        Queue.push (n_row, n_col) q;
        bounds := (n_row, n_col) :: !bounds;
      )
    )
  ) next
;;

let below_threshold grid threshold (row, col) =
  grid.(row).(col) < threshold
;;

let invert_cond cond loc =
  not (cond loc)
;;

let grid_get_next_no_collide grid collide loc =
  List.filter (fun (row, col) ->
    grid.(row).(col) = max_int
    && not collide.(row).(col)
  ) (get_adjacent grid loc)
;;

let grid_get_next grid loc =
  List.filter (fun (row, col) ->
    grid.(row).(col) = max_int
  ) (get_adjacent grid loc)
;;

let obstacle_grid_get_next obstacle grid loc =
  List.filter (fun (row, col) ->
    grid.(row).(col) = max_int
    && (not obstacle.(row).(col))
  ) (get_adjacent grid loc)
;;

let grid_get_next_cond cond grid loc =
  List.filter (fun (row, col) ->
    grid.(row).(col) = max_int
    && cond loc
  ) (get_adjacent grid loc)
;;

let simple_init q loc =
  Queue.push loc q;
;;

let list_init l q =
  List.iter (simple_init q) l
;;

let bfs init visit get_next =
  let q = Queue.create () in
    init q;
    let finished = ref false in
    while not !finished do
      begin try
        let item = Queue.pop q in
        let next = get_next item in
          visit item next q
      with _ -> finished := true
      end
    done
;;

let short_bfs init visit get_next should_terminate =
  let q = Queue.create () in
    init q;
    let finished = ref false in
    while not !finished do
      begin try
        let item = Queue.pop q in
        let next = get_next item in
          visit item next q;
          if should_terminate item then finished := true
      with _ -> finished := true
      end
    done
;;

