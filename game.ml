(* OCaml Starter for Halite III on Halite.io
   This code is public domain. There is no warranty.
 *)

open Type;;

let move_dirs = [North; East; South; West];;

let all_dirs = [North; East; South; West; Still];;

let dir_offset = function
| North -> (-1, 0)
| East -> (0, 1)
| South -> (1, 0)
| West -> (0, -1)
| Still -> (0, 0)
;;

let list_move_offsets = List.map dir_offset move_dirs;;

let time_seconds_elapsed_this_turn state =
  Unix.gettimeofday () -. state.last_update
;;

let wrap_grid_position (rows, cols) (row, col) =
  let wr =
    if row < 0 then
      row + rows
    else if row >= rows then
      row - rows
    else row
  in
  let wc =
    if col < 0 then
      col + cols
    else if col >= cols then
      col - cols
    else col 
  in
  wr, wc
;;

let wrap_ugrid_position grid position =
  let rows = Array.length grid in 
  let cols = Array.length grid.(0) in
  wrap_grid_position (rows, cols) position
;;

let wrap_position state position =
  wrap_grid_position (state.height, state.width) position
;;

let add_position (r1, c1) (r2, c2) = 
  (r1 + r2), (c1 + c2)
;;

let subtract_position (r1, c1) (r2, c2) =
  (r1 - r2), (c1 - c2)
;;

let directional_offset_position state position direction =
  wrap_position state (add_position position (dir_offset direction))
;;

let step_destination state entity direction =
  directional_offset_position state entity.position direction
;;

let abs_position (r, c) = (abs r, abs c);;

let row_col_distance state source target =
  let source' = wrap_position state source in
  let target' = wrap_position state target in
  let row, col = abs_position (subtract_position source' target') in
  (min row (state.height - row)), (min col (state.width - col))
;;

(* this logic was copied over from the Python starter, it has not been tested *)
let calculate_distance state source target =
  let source' = wrap_position state source in
  let target' = wrap_position state target in
  let row, col = abs_position (subtract_position source' target') in
  (min row (state.height - row)) + (min col (state.width - col))
;;

let rogue_distance state source target =
  let source' = wrap_position state source in
  let target' = wrap_position state target in
  let row, col = abs_position (subtract_position source' target') in
  max (min row (state.height - row)) (min col (state.width - col))
;;

let ships_of_player state player =
  List.filter (fun entity ->
      entity.entity_type = Ship
      && (entity.owner = player)
  ) state.ships
;;

let my_ships state =
  ships_of_player state state.my_id
;;

let not_my_ships state =
  List.filter (fun entity ->
      entity.entity_type = Ship
      && (entity.owner != state.my_id)
  ) state.ships
;;

let largest_single_not_my_ships state =
  let count = Array.map (fun _ -> 0) state.energy in
  List.iter (fun entity ->
    count.(entity.owner) <- count.(entity.owner) + 1
  ) state.ships;
  let best_score, best_player, _ = 
    Array.fold_left (fun (best_score, best, count) v ->
      if (count != state.my_id) then (
        if v > best then (v, count, count + 1)
        else (best_score, best, count + 1)
      )
      else (
        best_score, best, count + 1
      )
    ) (0, 0, 0) count
  in
    best_score
;;

let largest_single_ship_count state =
  let count = Array.map (fun _ -> 0) state.energy in
  List.iter (fun entity ->
    count.(entity.owner) <- count.(entity.owner) + 1
  ) state.ships;
  let best_score, best_player, _ = 
    Array.fold_left (fun (best_score, best, count) v ->
      if v > best then (v, count, count + 1)
      else (best_score, best, count + 1)
    ) (0, 0, 0) count
  in
    best_score
;;

let get_shipyard state player =
  List.find (fun entity ->
      entity.owner = player
  ) state.shipyards
;;

let my_shipyard_position state =
  (get_shipyard state state.my_id).position
;;

let dropoff_positions_of_player state player_id =
  List.map (fun dropoff -> dropoff.position) 
    (List.filter (fun dropoff -> dropoff.owner = player_id) state.dropoffs)
;;

let my_dropoff_positions state =
  dropoff_positions_of_player state state.my_id
;;

let position_not_owned state pos =
  let f e = e.position = pos in
  not ((List.exists f state.dropoffs) || (List.exists f state.shipyards))
;;

let my_bases state =
  (List.filter (fun e -> e.owner = state.my_id) ) (state.dropoffs @ state.shipyards)
;;

let is_my_base_position state position =
  List.exists (fun e ->
    e.owner = state.my_id && (e.position = position)
  ) (state.dropoffs @ state.shipyards)
;;

let p_ships_of_player state player =
  List.filter (fun p_ship ->
      p_ship.entity.entity_type = Ship
      && (p_ship.entity.owner = player)
  ) state.persist.p_ships
;;

let my_p_ships state =
  p_ships_of_player state state.my_id
;;

let my_p_ships_move_not_decided state =
  List.filter (fun p_ship ->
      p_ship.entity.entity_type = Ship
      && (not p_ship.move_decided)
      && (p_ship.entity.owner = state.my_id)
  ) state.persist.p_ships
;;

let int_to_dir = function
  | 0 -> North
  | 1 -> East
  | 2 -> South
  | 3 -> West
  | _ -> Still
;;

let random_nonstill_dir () =
  int_to_dir (Random.int 4)
;;

let my_halite state =
  state.energy.(state.my_id)
;;

let get_shipyard state player =
  List.find (fun entity ->
      entity.owner = player
  ) state.shipyards
;;

let shipyard_occupied state player =
  try (
    let shipyard = get_shipyard state player in
      let result = (List.exists (fun ship ->
        ship.position = shipyard.position
      ) state.ships)
      in
        result
  )
  with e -> 
    Debug.debug (Printexc.to_string e);
    true
;;

let enemy_at state position =
  List.exists (fun ship -> 
    (ship.owner != state.my_id )
    && (ship.position = position)
  ) state.ships
;;

let adjacent state pos1 pos2 =
  calculate_distance state pos1 pos2 = 1
;;

let enemy_could_occupy state position =
  List.exists (fun ship -> 
    (ship.owner != state.my_id )
    && ((ship.position = position) || adjacent state ship.position position)
  ) state.ships
;;

let enemy_high_risk_occupy state position =
  List.exists (fun p_ship -> 
(*    if p_ship.prev_position = p_ship.entity.position then *)
    if p_ship.turns_stationary > 0 then
      p_ship.entity.position = position
    else (
      let ship = p_ship.entity in
      (ship.owner != state.my_id )
      && ((ship.position = position) || adjacent state ship.position position)
    )
  ) state.persist.p_ships
;;

let new_p_ship ship =
 {
  entity = ship;
  desired_moves = [];
  prev_position = ship.position;
  move_decided = false;
  assignment = Collect;
  medium_destination = None;
  target = None;
  plan = None; 
  possible_moves = [];
  inspiration_relevant = false;
  could_inspire = [];
  last_collection = None;
  current_collection = None;
  turns_stationary = 0;
  crash_risk_wait = 0;
  in_danger = false;
  chased = false;
  chaser = false;
  contested = [];
  contested_target = None;
 }
;;

let add_p_ship state ship =
  state.persist.p_ships <- new_p_ship ship :: state.persist.p_ships
;;

let empty_collection_cycle begin_turn =
  {
    turn_begun = begin_turn;
    turn_completed = -1;
    amount_returned = -1;
  }
;;

let print_crash_risks state =
  Debug.debug "crash_risk_unoccupied: ";
  Array.iteri (fun player arr ->
    Debug.debug ("player " ^ (string_of_int player) ^ "\n");
    Array.iteri (fun turn (i1, i2) ->
      Debug.debug (Printf.sprintf "%d : %04d, %04d, %f\n" turn i1 i2 (float_of_int i1 /. float_of_int i2));
    ) arr
  ) state.persist.crash_risk_unoccupied;
  Debug.debug "crash_risk_occupied: ";
  Array.iteri (fun player arr ->
    Debug.debug ("player " ^ (string_of_int player) ^ "\n");
    Array.iteri (fun turn (i1, i2) ->
      Debug.debug (Printf.sprintf "%d : %04d, %04d, %f\n" turn i1 i2 (float_of_int i1 /. float_of_int i2));
    ) arr
  ) state.persist.crash_risk_occupied;
;;

let update_p_ship_risk_behaviour_data state p_ship updated_entity =
  if state.persist.danger_maps = [||] then ()
  else (
    let danger_map = state.persist.danger_maps.(p_ship.entity.owner) in
    (*let source_r, source_c = p_ship.entity.position in*)
    let dest_r, dest_c = updated_entity.position in
    (*let source_risk = danger_map.(source_r).(source_c) in*)
    let dest_risk = danger_map.(dest_r).(dest_c) in
    let still = (p_ship.entity.position = updated_entity.position) in
    if still && dest_risk > 2 then ( (* still while not in danger *)
      let target_array = state.persist.crash_risk_unoccupied.(updated_entity.owner) in
      let turn = min state.persist.risk_turns p_ship.crash_risk_wait in
      let prev_risk, prev_cower = target_array.(turn) in
      target_array.(turn) <- (prev_risk, prev_cower + 1)
    )
    else if dest_risk < 2 then (
      (*let dest_friendly_occupied = friendly_occupied state p_ship (dest_r, dest_c) in*)
      if not (still (*|| dest_friendly_occupied*)) then (
        let dest_enemy_occupied = dest_risk = 0 in
        let target_array = (if dest_enemy_occupied then state.persist.crash_risk_occupied else state.persist.crash_risk_unoccupied).(updated_entity.owner) in
        let turn = min state.persist.risk_turns p_ship.crash_risk_wait in
        let prev_risk, prev_cower = target_array.(turn) in
        target_array.(turn) <- (prev_risk + 1, prev_cower)
      )
    )
  )
;;

let update_p_ship_collection_cycle_data state p_ship updated_entity =
  let dropoff_search = state.persist.all_dropoff_searches.(updated_entity.owner) in
  let row, col = updated_entity.position in
  let on_dropoff = dropoff_search.(row).(col) < 1 in
  if on_dropoff && (p_ship.entity.halite > 0 || (p_ship.current_collection = None)) then (
    match p_ship.current_collection with
    | None -> p_ship.current_collection <- Some (empty_collection_cycle state.turn)
    | Some collection ->
      collection.turn_completed <- state.turn;
      collection.amount_returned <- p_ship.entity.halite;
      p_ship.last_collection <- Some collection;
      p_ship.current_collection <- Some (empty_collection_cycle state.turn);
  )
;;

let get_adjacent grid loc =
  let size = (Array.length grid, Array.length grid.(0)) in
  List.map (fun offset -> 
    wrap_grid_position size (add_position loc offset)
  ) list_move_offsets
;;

let find_crash_site state crashed_ships p_ship =
  let adj = get_adjacent state.map p_ship.entity.position in
  let positions = p_ship.entity.position :: adj in
  List.fold_left (fun acc position ->
    (*Debug.debug "a";*)
    match acc with
    | None ->
      begin try (
        let e_ship = List.find (fun crashed ->
          (*Debug.debug "b";*)
          let distance = calculate_distance state p_ship.entity.position position in
          (not (crashed.entity.owner = p_ship.entity.owner && (crashed.entity.id = p_ship.entity.id)))
          && (distance < 2) 
          &&
          (
            let row, col = position in
            (*Debug.debug "e";*)
            (*Debug.debug (Printf.sprintf "position = %d, %d : map size = %d : prev_map size = %d\n" row col (Array.length state.map) (Array.length state.prev_map));*)
            let result = state.map.(row).(col) - state.prev_map.(row).(col) >= p_ship.entity.halite + crashed.entity.halite in
            (*Debug.debug "f";*)
            result;
          )
        ) crashed_ships in
        Some (e_ship, position)
      )
      with Not_found -> acc
      end
    | Some _ -> acc
  ) None positions
;;

let update_crash_risk_attack_halite state chaser position_attacked chased =
  let attacker = chaser.entity.owner in
  let attack_row, attack_col = position_attacked in
  let attack_halite = state.map.(attack_row).(attack_col) in
  let move_cost = attack_halite / state.const.move_cost_ratio in
  let prize = chased.entity.halite + move_cost + attack_halite in
  state.persist.crash_risk_attack_halite.(attacker) <-
    prize :: state.persist.crash_risk_attack_halite.(attacker);
;;

let extract_crash_data state p_ships =
  List.iter (fun p_ship ->
    match find_crash_site state p_ships p_ship with
    | None -> ()
    | Some (e_ship, position) ->
      (*Debug.debug "c";*)
      let still = (p_ship.entity.position = position) in
      if not still then (
        let danger_map = state.persist.danger_maps.(p_ship.entity.owner) in
        let dest_r, dest_c = position in
        let dest_risk = danger_map.(dest_r).(dest_c) in
        let dest_enemy_occupied = dest_risk = 0 in
        let target_array = (if dest_enemy_occupied then state.persist.crash_risk_occupied else state.persist.crash_risk_unoccupied).(p_ship.entity.owner) in
        let turn = min state.persist.risk_turns p_ship.crash_risk_wait in
        let prev_risk, prev_cower = target_array.(turn) in
        target_array.(turn) <- (prev_risk + 1, prev_cower);
        if dest_enemy_occupied then (
          update_crash_risk_attack_halite state p_ship position e_ship
        )
      )
  ) p_ships
;;

let ship_in_danger state ship =
  List.exists (fun ship2 ->
    (not (ship2.owner = ship.owner && (ship2.id = ship.id)))
    && (not (ship2.owner = ship.owner))
    && (calculate_distance state ship2.position ship.position < 3)
  ) state.ships
;;

let clear_chaser_flags p_ships =
  List.iter (fun p_ship -> 
    p_ship.chaser <- false;
  ) p_ships
;;

let clear_empty_chased_flags p_ships =
  List.iter (fun p_ship -> 
    if p_ship.entity.halite = 0 then
      p_ship.chaser <- false;
  ) p_ships
;;

let update_chasers_and_chased state =
  List.iter (fun p_ship ->
    try (
      let chaser = List.find (fun e_ship ->
        e_ship.entity.owner != p_ship.entity.owner
        && (
          p_ship.prev_position = e_ship.entity.position
        )
      ) state.persist.p_ships in
      p_ship.chased <- true;
      chaser.chaser <- true;
      update_crash_risk_attack_halite state chaser chaser.entity.position p_ship;
      (*
      let attack_row, attack_col = chaser.entity.position in
      let attack_halite = state.map.(attack_row).(attack_col) in
      let move_cost = attack_halite / state.const.move_cost_ratio in
      let prize = p_ship.entity.halite + move_cost + attack_halite in
      state.persist.crash_risk_attack_halite.(attacker) <-
        prize :: state.persist.crash_risk_attack_halite.(attacker);
      *)
    )
    with Not_found -> ()
  ) state.persist.p_ships
;;

let update_p_ships state =
  clear_chaser_flags state.persist.p_ships;
  let ships = state.ships in
  Debug.debug (Printf.sprintf "my ships length = %d\n" (List.length(my_ships state)));

  let survived, crashed = List.partition (fun p_ship ->
    List.exists (fun ship ->
        (ship.owner = p_ship.entity.owner)
        && (ship.id = p_ship.entity.id)
    ) ships
  ) state.persist.p_ships in

  state.persist.p_ships <- survived;

  (*Debug.debug "\nd";*)
  extract_crash_data state crashed;
  (*Debug.debug "e\n";*)

  List.iter (fun ship ->
    begin try let clone_ship = List.find (fun p_ship ->
        p_ship.entity.id = ship.id
    ) state.persist.p_ships in
     (
      update_p_ship_collection_cycle_data state clone_ship ship;
      update_p_ship_risk_behaviour_data state clone_ship ship;
      if (clone_ship.entity.position = ship.position) then (
        clone_ship.turns_stationary <- clone_ship.turns_stationary + 1;
        clone_ship.crash_risk_wait <- clone_ship.crash_risk_wait + 1;
      )
      else (
        clone_ship.turns_stationary <- 0;
        clone_ship.crash_risk_wait <- 0;
      );
      if (not clone_ship.in_danger) && (ship_in_danger state ship) then (
        clone_ship.crash_risk_wait <- 0;
      );
      clone_ship.prev_position <- clone_ship.entity.position;
      clone_ship.entity <- ship;
      (*Debug.debug (Printf.sprintf "found ship: owner = %d, id = %d\n" ship.owner ship.id);*)
     )
    with Not_found ->
      (*Debug.debug (Printf.sprintf "new ship: owner = %d, id = %d\n" ship.owner ship.id);*)
      add_p_ship state ship
    end
  ) ships;

  update_chasers_and_chased state;
  clear_empty_chased_flags state.persist.p_ships;

  Debug.debug (Printf.sprintf "my p_ships length = %d\n" (List.length (my_p_ships state)));
  print_crash_risks state;

;;

let turns_remaining state = state.const.max_turns - state.turn;;

let target_position = function
  | Base (entity, _) -> entity.position
  | Harvest (p_ship, _) -> p_ship.entity.position
  | Crash_site (position) -> position
;;

let new_constant_set () = {
  capture_enabled = false;
  capture_radius = 0;
  default_map_height = 0;
  default_map_width = 0;
  dropoff_cost = 0;
  dropoff_penalty_ratio = 0;
  extract_ratio = 0;
  factor_exp_1 = 0.0;
  factor_exp_2 = 0.0;
  initial_energy = 0;
  inspiration_enabled = false;
  inspiration_radius = 0;
  inspiration_ship_count = 0;
  inspired_bonus_multiplier = 0.0;
  inspired_extract_ratio = 0;
  inspired_move_cost_ratio = 0;
  max_cell_production = 0;
  max_energy = 0;
  max_players = 0;
  max_turns = 0;
  max_turn_threshold = 0;
  min_cell_production = 0;
  min_turns = 0;
  min_turn_threshold = 0;
  move_cost_ratio = 0;
  new_entity_energy_cost = 0;
  persistence = 0.0;
  ships_above_for_capture = 0;
  strict_errors = false;
  game_seed = 0
};;

let new_state () = {
  tokens = [];
  num_players = 0;
  my_id = -1;
  width = 1;
  height = 1;
  map = [||];
  prev_map = [||];
  shipyards = [];
  ships = [];
  dropoffs = [];
  energy = [||];
  turn = -1;
  last_update = Unix.gettimeofday();
  persist = { 
    p_ships = [];
    desire_map = [||];
    intent_map = [||];
    condense_factor = 4;
    inspired_condensed_map = [||];
    condensed_map = [||];
    condensed_area_under_threat = [||];
    under_threat = [||];
    searches = [||];
    dropoff_search = [||];
    return_search = [||];
    safe_dropoff_search = [||];
    obstacles = [||];
    obstacles_updated = false;
    crash_search = [||];
    weighted_dropoff_search = [||];
    all_dropoff_searches = [||];
    all_enemy_search = [||];
    ships_near = [||];
    half_sum_threshold = 0;
    percentile_thresholds = [];
    next_dropoff_positions = [];
    constructed_this_turn = 0;
    construction_decided = false;
    targets = [];
    magic = Magic.new_magic None;
    stupidity = 0.;
    total_halite_available = 0;
    save_for_dropoff = false;
    crash_risk_occupied = [||];
    crash_risk_unoccupied = [||];
    danger_maps = [||];
    risk_turns = 8;
    crash_risk_attack_halite = [||];
  };
  const = new_constant_set();
};;

