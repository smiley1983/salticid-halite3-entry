open Type;;

let copy_2d_array outer =
  Array.map (Array.map (fun x -> x)) outer
;;

let copy_position (row, col) = (row, col);;

let copy_entity e =
 {e with
   position = copy_position e.position;
   halite = e.halite
 }
;;

let copy_entities entities =
  List.map copy_entity entities
;;

let copy_game_state state =
  {state with
    tokens = [];
    map = copy_2d_array state.map;
    ships = copy_entities state.ships;
    shipyards = copy_entities state.shipyards;
    dropoffs = copy_entities state.dropoffs;
    energy = Array.map (fun i -> i) state.energy;
  }
;;

let make_f_ship turn future_turns ship =
  f_entity = ship;
  f_command = Array.make future_turns None;
  exist_from = turn;
;;

let new_future_search state = 
  let future_turns = state.width * 3 / 2 in
  {
    halite = Array.init future_turns (fun i ->
      if i = 0 then copy_2d_array state.map
      else Array.map (Array.map (fun _ -> 0)) state.map
    );
    occupied = Array.init future_turns (fun i ->
      Array.map (Array.map (fun _ -> false)) state.map
    )
    visited = Array.map (Array.map (fun _ -> false)) state.map in
    generate = Array.make future_turns false;
    f_ships = Array.map (make_f_ship state.turn future_turns) state.ships;
    offset = 0;
    future_turns = future_turns;
  }
;;

let cancel_future future f_ship =
  Array.iteri (fun index' command ->
    match command with
    | None -> ()
    | Some f_command ->
      begin match f_command with
      | FConstruct (id) -> ()
      | FMove (data) ->
        let index = (index' + future.offset) mod future.future_turns in
        let row, col = data.f_pos in
        future.halite.(index).(row).(col) <- future.halite.(index).(row).(col) + data.f_collect
        future.occupied.(index).(row).(col) <- false;
      end
  ) f_ship.f_commands
;;

let ship_future_search state future f_ship =
  cancel_future future f_ship;
;;



(*

let new_sim_state state = {
  real = state;
  sim_map = copy_2d_array state.map;
  sim_ships = copy_entities state.ships;
  sim_shipyards = copy_entities state.shipyards;
  sim_dropoffs = copy_entities state.dropoffs;
};;
*)

