open Printf;;

let write_pnm filename data =
  let width = Array.length data.(0) in
  let height = Array.length data in 
  let chan = open_out filename in
  fprintf chan "P3 %d %d 255\n" width height;
  Array.iter 
    (Array.iter (fun (r, g, b) -> 
      fprintf chan "%d %d %d\n" r g b;
    )) data;
  ignore (close_out chan)
;;

