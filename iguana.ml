open Type;;

let file_count = ref 0;;

let compare_float_desc (f1:float) (f2:float) : int =
  if f1 < f2 then 1
  else if f2 < f1 then -1
  else 0
;;

let compare_float_asc (f1:float) (f2:float) : int =
  if f1 < f2 then -1
  else if f2 < f1 then 1
  else 0
;;

let map_2d_empty_list_array_array outer_array =
  Array.map (fun arr -> 
    Array.map (fun _ -> 
      []
    ) arr
  ) outer_array
;;

let clear_2d_list_array_array outer =
  Array.iter (fun inner ->
    Array.iteri (fun ic cell ->
      inner.(ic) <- []
    ) inner
  ) outer
;;

let sum_grid grid =
  Array.fold_left (fun row_acc row ->
    Array.fold_left (fun col_acc col -> col_acc + col) row_acc row
  ) 0 grid
;;

let debug_int_grid outer =
  Array.iter (fun inner ->
   Debug.debug "\n";
    Array.iter (fun cell ->
      Debug.debug (Printf.sprintf " %02x" (min 255 cell));
  ) inner ) outer;
  Debug.debug "\n\n";
;;

let largest_grid_value outer =
  Array.fold_left (fun acc inner ->
    let v = Array.fold_left (fun iacc iv -> max iacc iv) min_int inner in
      max acc v
  ) min_int outer
;;

let normalise_int_grid outer upper =
  let largest = largest_grid_value outer in
  (*Debug.debug (Printf.sprintf "Largest value in grid = %d\n" largest);*)
  let factor = (float_of_int upper) /. (float_of_int largest) in
  Array.map (Array.map (fun cell ->
    int_of_float (ceil (float_of_int cell *. factor))
  )) outer
;;

let sum_rectangle outer start_row start_col num_rows num_cols =
  let sum = ref 0 in
  for row_num = start_row to start_row + num_rows - 1 do
    let row = outer.(row_num) in
    for col_num = start_col to start_col + num_cols - 1 do
      sum := !sum + row.(col_num)
    done
  done;
  !sum
;;

let squashed_int_grid int_grid row_factor col_factor =
  let grid_rows = Array.length int_grid in
  let grid_cols = Array.length int_grid.(0) in
  (*Debug.debug (Printf.sprintf "squashed_int_grid: rows, cols = %d, %d : row_factor, col_factor = %d, %d\n" grid_rows grid_cols row_factor col_factor);*)
  if ((grid_rows mod row_factor) != 0) 
  || ((grid_cols mod col_factor) != 0) then (
    (*Debug.debug (Printf.sprintf "WARNING: skewed squashed_int_grid because rows, cols = %d, %d while row_factor, col_factor = %d, %d\n" grid_rows grid_cols row_factor col_factor);*)
  );
  
  let target_rows = grid_rows / row_factor in
  let target_cols = grid_cols / col_factor in
  Array.init target_rows (fun row ->
    Array.init target_cols (fun col ->
      let start_row = row * row_factor in
      let start_col = col * col_factor in
      sum_rectangle int_grid start_row start_col row_factor col_factor
    )
  )
;;

let clear_intent_map state =
  (*clear_2d_list_array_array state.persist.intent_map*)
  state.persist.intent_map <- map_2d_empty_list_array_array state.map
;;

let clear_desire_map state =
  (*clear_2d_list_array_array state.persist.desire_map*)
  state.persist.desire_map <- map_2d_empty_list_array_array state.map
;;

let move_cost state (row, col) =
  let halite = state.map.(row).(col) in
    int_of_float (ceil ((float_of_int halite) /. 10.))
;;

let cannot_move state ship =
  let move_cost = move_cost state ship.position in
    move_cost > ship.halite
;;

let add_desire state p_ship desire =
  (*Debug.debug (Printf.sprintf "Adding desire: %s\n" (Debug.string_of_desire desire));*)
  p_ship.desired_moves <- desire :: p_ship.desired_moves;
  let row, col = desire.destination in
    state.persist.desire_map.(row).(col) <- desire :: state.persist.desire_map.(row).(col)
;;

let desire_construct p_ship weight =
  {
    p_ship = p_ship;
    command = Construct (p_ship.entity.id);
    destination = p_ship.entity.position;
    weight = weight;
  }
;;

let desire_stay_still p_ship weight =
  {
    p_ship = p_ship;
    command = Move (p_ship.entity.id, Still);
    destination = p_ship.entity.position;
    weight = weight;
  }
;;

let desire_move state p_ship direction weight =
  let dest = Game.step_destination state p_ship.entity direction in
  {
    p_ship = p_ship;
    command = Move (p_ship.entity.id, direction);
    destination = dest;
    weight = weight;
  }
;;

let percentile_normalise_mul mul l =
  let minimum = List.fold_left (fun acc (_, v) -> min acc v) 0 l in
  let adjusted = List.map (fun (d, v) -> d, (v - minimum) + 1) l in
  let sum a (_, b) = a + b in
  let total = List.fold_left sum 0 adjusted in
  let weights = List.map (fun (d, v) ->
    let flt = float_of_int v /. float_of_int total in
    d, (flt *. 100. *. mul)
  ) adjusted in
    weights
;;

let multiply_weights_int mul l =
  List.map (fun (d, v) ->
    d, v *. mul
  ) l
(*
  let minimum = List.fold_left (fun acc (_, v) -> min acc v) 0. l in
  let adjusted = List.map (fun (d, v) -> d, (v -. minimum) +. 1.) l in
  let sum a (_, b) = a +. b in
  let total = List.fold_left sum 0. adjusted in
  let weights = List.map (fun (d, v) ->
    let flt = v /. float_of_int total in
    d, (flt *. 100. *. mul)
  ) adjusted in
    weights
*)
;;

let percentile_normalise_mul mul l =
  let minimum = List.fold_left (fun acc (_, v) -> min acc v) 0 l in
  let adjusted = List.map (fun (d, v) -> d, (v - minimum) + 1) l in
  let sum a (_, b) = a + b in
  let total = List.fold_left sum 0 adjusted in
  let weights = List.map (fun (d, v) ->
    let flt = float_of_int v /. float_of_int total in
    d, (flt *. 100. *. mul)
  ) adjusted in
    weights
;;

(*
let add_desires state p_ship directions =
  Debug.debug (Printf.sprintf "Add desires for p_ship %s\n" (Debug.string_of_p_ship p_ship));
  Debug.debug (Printf.sprintf "Num directions = %d\n" (List.length directions));
  let weight = 100 / List.length directions in
  List.iter (fun dir ->
    let desire = desire_move state p_ship dir weight in
    add_desire state p_ship desire
  ) directions;
  List.iter (fun dir ->
    if not (List.exists (fun wanted -> dir = wanted) directions) then (
      let desire = desire_move state p_ship dir 0 in
      let low_weight = 
        if dir = Still then (
          if desire.destination = (Game.my_shipyard_position state) then 1 
          else 3
        ) 
        else 2 
      in
      let desire' = desire_move state p_ship dir low_weight in
        add_desire state p_ship desire'
    )
  ) Game.all_dirs
;;
*)

let add_evenly_weighted_desires state p_ship directions multiplier =
  (*Debug.debug (Printf.sprintf "Add desires for p_ship %s\n" (Debug.string_of_p_ship p_ship));*)
  (*Debug.debug (Printf.sprintf "Num directions = %d\n" (List.length directions));*)
  let weight = (100. *. multiplier /. (float_of_int (List.length directions))) in
  List.iter (fun dir ->
      (*Debug.debug (Printf.sprintf "Adding primary desire dir %s\n" (Debug.string_of_dir dir));*)
    let desire = desire_move state p_ship dir weight in
    add_desire state p_ship desire
  ) directions;
  List.iter (fun dir ->
    if not (List.exists (fun wanted -> dir = wanted) directions) then (
      (*Debug.debug (Printf.sprintf "Adding secondary desire dir %s\n" (Debug.string_of_dir dir));*)
      let desire = desire_move state p_ship dir 0. in
      let low_weight = 
        if dir = Still then (
          if desire.destination = (Game.my_shipyard_position state) then 1.
          else 3.
        ) 
        else 2.
      in
      let desire' = desire_move state p_ship dir low_weight in
        add_desire state p_ship desire'
    )
  ) Game.all_dirs
;;

let add_weighted_desires state p_ship directions multiplier =
  (*Debug.debug (Printf.sprintf "Add weighted desires for p_ship %s\n" (Debug.string_of_p_ship p_ship));*)
  (*Debug.debug (Printf.sprintf "Num directions = %d\n" (List.length directions));*)
  List.iter (fun (dir, weight) ->
      (*Debug.debug (Printf.sprintf "Adding primary desire dir %s\n" (Debug.string_of_dir dir));*)
    let desire = desire_move state p_ship dir (weight *. multiplier) in
    add_desire state p_ship desire
  ) directions;
  if not (cannot_move state p_ship.entity) then List.iter (fun dir ->
    if not (List.exists (fun (wanted, _) -> dir = wanted) directions) then (
      (*Debug.debug (Printf.sprintf "Adding secondary desire dir %s\n" (Debug.string_of_dir dir));*)
      let desire = desire_move state p_ship dir 0. in
      let low_weight = 
        if dir = Still then (
          if desire.destination = (Game.my_shipyard_position state) then 1.
          else 3.
        ) 
        else 2.
      in
      let desire' = desire_move state p_ship dir low_weight in
        add_desire state p_ship desire'
    )
  ) Game.all_dirs
;;

let eval_add_desires state p_ship eval adjust =
  (*Debug.debug (Printf.sprintf "eval_add_desires for ship: %s\n" (Debug.string_of_p_ship p_ship));*)
  let move_values = List.map (fun dir ->
    let target = Game.directional_offset_position state p_ship.entity.position dir in
    dir, eval target
  ) Game.all_dirs in
  let weights = adjust move_values in
  List.iter (fun (dir, weight) ->
    let desire = desire_move state p_ship dir weight in
    add_desire state p_ship desire
  ) weights
;;


let add_construction_desires state p_ship =
  (*Debug.debug (Printf.sprintf "Add construction desires for p_ship %s\n" (Debug.string_of_p_ship p_ship));*)
  let desire = desire_construct p_ship 1000. in
  add_desire state p_ship desire;
  List.iter (fun dir ->
    if not (dir = Still) then (
      let low_weight = 2. in
      let desire' = desire_move state p_ship dir low_weight in
        add_desire state p_ship desire'
    )
  ) Game.all_dirs
;;

let weight_directions state p_ship f =
  let qualities = List.map (fun dir ->
    let position = Game.directional_offset_position state p_ship.entity.position dir in
    dir, (f position)
  ) Game.all_dirs in
  let minimum = List.fold_left (fun acc (_, v) -> min acc v) 0. qualities in
  let adjusted = List.map (fun (d, v) -> d, (v -. minimum) +. 1.) qualities in
    adjusted
;;

(* return the directions whose distances are less than or equal to the start 
  distance. *)
let manhattan_navigate state source target =
  let dstart = Game.calculate_distance state source target in
  List.filter (fun dir ->
    let test_pos = Game.directional_offset_position state source dir in
    let dfinish = Game.calculate_distance state test_pos target in
      dfinish <= dstart
  ) Game.move_dirs
;;

(*
let best_directions state compare p_ship directions initial_score =
  let found_best =
    List.fold_left (fun best_set dir ->
      let row, col = Game.step_destination state p_ship.entity dir in
      let score = state.map.(row).(col) in
      let comparison = compare score best_set in
      if comparison > 0 then
        [(score, dir)]
      else if comparison = 0 then
        (score, dir) :: best_set
      else best_set
    ) [(initial_score, List.nth directions 0)] directions
  in
  List.map (fun (_, dir) -> dir) found_best
;;
*)
(*
let explore_dirs state p_ship =
  let dirs = Game.move_dirs in
  (*let row, col = p_ship.entity.position in*)
  let initial_score = (*state.map.(row).(col)*) -100000 in
  let compare:(int -> (int * 'a) list -> int) = fun a b -> (List.fold_left (fun acc b' ->
    let test_val = a - b' in
    if test_val > acc then test_val
    else acc
  ) (-100000) (List.map (fun (b', _) -> b') b)) in
  best_directions state compare p_ship dirs initial_score
;;

let return_dirs state p_ship =
  let dirs = manhattan_navigate state p_ship.entity.position (Game.my_shipyard_position state) in
  let row, col = p_ship.entity.position in
  let initial_score = state.map.(row).(col) in
  let compare:(int -> (int * 'a) list -> int) = fun a b -> (List.fold_left (fun acc b' ->
    let test_val = b' - a in
    if test_val > acc then test_val
    else acc
  ) (-100000) (List.map (fun (b', _) -> b') b)) in
  best_directions state compare p_ship dirs initial_score
;;
*)

let sum_desire_weight =
  List.fold_left (fun acc desire ->
    acc +. desire.weight
  ) 0.
;;

let flatten_2d_array arr =
  Array.fold_left (fun acc iarr -> Array.append acc iarr) [||] arr
;;

let get_desire_cells arr =
  let result = ref [] in
  Array.iter (Array.iter (fun cell -> 
    if (List.length cell) > 0 then result := cell :: !result
  )) arr;
  !result
;;

let get_undecided_desire_cells state =
  let result = ref [] in
  Array.iteri (fun ir row -> Array.iteri (fun ic cell -> 
    if (List.length cell) > 0 
    && (List.length state.persist.intent_map.(ir).(ic) = 0) then (
      result := cell :: !result
    )
  ) row) state.persist.desire_map;
  !result
;;

let finalise_cell_move state desires =
  if (List.length desires) > 0 then (
    (*Debug.debug (Printf.sprintf "finalise_cell_move desires: %s\n" (Debug.string_of_desire_list desires));*)
    let sorted = List.sort (fun a b ->
      if a.p_ship.move_decided && (not b.p_ship.move_decided) then 1
      else if (not a.p_ship.move_decided) && (b.p_ship.move_decided) then -1
      else (*(int_of_float (1_000_000_000. *. (b.weight -. a.weight)))*)
        compare_float_desc a.weight b.weight
    ) desires in
    (*Debug.debug (Printf.sprintf "finalise_cell_move sorted desires: %s\n" (Debug.string_of_desire_list sorted));*)
    match sorted with
    | desire :: tail ->
      if desire.p_ship.move_decided then None
      else (
        let row, col = desire.destination in
        (*Debug.debug (Printf.sprintf "Move chosen with destination %d, %d\n" row col);*)
        state.persist.desire_map.(row).(col) <- tail;
        state.persist.intent_map.(row).(col) <- desire :: state.persist.intent_map.(row).(col);
        Some desire
      )

    | [] -> None
  )
  else (
    None
  )
;;

let finalise_cell_ship_move state desires p_ship =
  if (List.length desires) > 0 then (
    let sorted = List.rev (List.sort (fun a b ->
      if a.p_ship.move_decided && (not b.p_ship.move_decided) then -1
      else if (not a.p_ship.move_decided) && (b.p_ship.move_decided) then 1
      else if a.p_ship.entity.id = p_ship.entity.id && (b.p_ship.entity.id != p_ship.entity.id) then -1
      else if (not (a.p_ship.entity.id = p_ship.entity.id)) && (b.p_ship.entity.id = p_ship.entity.id) then 1
      else (*(int_of_float (a.weight -. b.weight))*)
        compare_float_asc a.weight b.weight
    ) desires) in
    match sorted with
    | desire :: tail ->
      if desire.p_ship.move_decided then None
      else (
        let row, col = desire.destination in
        (*Debug.debug (Printf.sprintf "Move chosen with destination %d, %d\n" row col);*)
        state.persist.desire_map.(row).(col) <- tail;
        state.persist.intent_map.(row).(col) <- desire :: state.persist.intent_map.(row).(col);
        Some desire
      )

    | [] -> None
  )
  else (
    None
  )
;;

let extract_intent_commands state =

  (*Debug.debug "extract_intent_commands\n";*)
  let commands = ref [] in

  Array.iter (Array.iter (function
    | desire :: tail -> 
      commands := desire.command :: !commands;
      (*
      if List.length tail > 0 then (
        (*Debug.debug (Printf.sprintf "Intent map: desire has %d length tail: %s\n" (List.length tail) (Debug.string_of_desire desire));*)
        List.iter (fun des' ->
          (*Debug.debug (Printf.sprintf "tail desire: %s\n" (Debug.string_of_desire des'));*)
        ) tail;
      )
      *)
    | _ -> ()
  )) state.persist.intent_map;

  !commands

;;

let all_desires_belong_to p_ship desires =
  not (List.exists (fun desire -> desire.p_ship.entity.id != p_ship.entity.id) desires)
;;

let rec amend_suicide_commands state commands = 
  (* Report conflicts *)
  let did_change = ref false in
  Array.iter (Array.iter (fun cell ->
    if List.length cell > 1 then (
      (*Debug.debug "Order conflict";*)
      (*
      List.iter (fun desire ->
        let row, col = desire.destination in
          (*Debug.debug (Printf.sprintf "Desination: %d, %d\n%s" row col (Debug.string_of_desire desire));*)
      ) cell
      *)
    )
  )) state.persist.intent_map;
  (* Report ships without commands and cancel orders if necessary *)
  List.iter (fun p_ship ->
    if not p_ship.move_decided then (
      (*Debug.debug (Printf.sprintf "Ship with no commands: %s\n" (Debug.string_of_p_ship p_ship));*)
      check_cancel_conflicts state p_ship true;
      did_change := true;
    )
  ) (Game.my_p_ships state);
  if !did_change then amend_suicide_commands state (extract_intent_commands state)
  else commands

and try_conflict_swap state p_ship desire =
  try (
    if not (
      (cannot_move state p_ship.entity) 
      || ((p_ship.assignment = Return && (desire.p_ship.assignment = Return))
          && (p_ship.entity.halite >= desire.p_ship.entity.halite)
         )
      || ((p_ship.assignment = Collect && (desire.p_ship.assignment = Collect))
         )
    ) then (
      let swap_desire = List.find (fun desire' ->
        desire'.destination = desire.p_ship.entity.position
      ) p_ship.desired_moves in
      let row, col = swap_desire.destination in
      let swap_row, swap_col = swap_desire.p_ship.entity.position in
      (*Debug.debug (Printf.sprintf "Tail desires = %d\n" (List.length state.persist.intent_map.(swap_row).(swap_col)));*)
      if (List.length state.persist.intent_map.(swap_row).(swap_col)) > 0 then (
        List.iter (fun cancel_desire ->
          check_cancel_conflicts state cancel_desire.p_ship false;
        ) state.persist.intent_map.(swap_row).(swap_col);
      );

(*
      if (List.length state.persist.intent_map.(row).(col)) > 0 then (
        List.iter (fun cancel_desire ->
          check_cancel_conflicts state cancel_desire.p_ship false;
        ) state.persist.intent_map.(row).(col);
      );
*)
      state.persist.intent_map.(row).(col) <- [swap_desire];
      p_ship.move_decided <- true;
      (*Debug.debug (Printf.sprintf "conflict_swap worked:\n    %s\n    %s\n" (Debug.string_of_desire desire) (Debug.string_of_desire swap_desire));*)
      true
      (*
      if (List.length state.persist.intent_map.(swap_row).(swap_col)) = 0 then (
        state.persist.intent_map.(row).(col) <- swap_desire;
        p_ship.move_decided <- true;
        Debug.debug "conflict_swap worked\n";
        true
      )
      else (
        List.iter (fun cancel_desire ->
          check_cancel_conflicts state cancel_desire.p_ship false;
        ) state.persist.intent_map.(swap_row).(swap_col);
        state.persist.intent_map.(row).(col) <- swap_desire;
        p_ship.move_decided <- true;
      )
      *)
    )
    else (
      false
    )
  )
  with Not_found -> false

and check_cancel_conflicts state p_ship allow_swap =
  (*Debug.debug (Printf.sprintf "check_cancel_conflicts for %s\n" (Debug.string_of_p_ship p_ship));*)
  let row, col = p_ship.entity.position in
  let len = List.length (state.persist.intent_map.(row).(col)) in
  if len > 0 && (
    allow_swap && (len = 1) && (try_conflict_swap state p_ship (List.nth state.persist.intent_map.(row).(col) 0))) then ()
  else (
    let desire = desire_stay_still p_ship 100_000. in
    let cancel_desires = state.persist.intent_map.(row).(col) in
    if (all_desires_belong_to p_ship state.persist.intent_map.(row).(col)) then ()
    else (
      List.iter (fun cancel_desire ->
        (*Debug.debug (Printf.sprintf "Cancelling desire %s\n" (Debug.string_of_desire cancel_desire));*)
        check_cancel_conflicts state cancel_desire.p_ship allow_swap
      ) cancel_desires;
    );
    state.persist.intent_map.(row).(col) <- [desire];
    p_ship.move_decided <- true; 
  )
;;


let maximum_desire p_ship =
  match (List.fold_left (fun acc desire ->
    match acc with
    | None -> Some desire
    | Some best -> 
      if desire.weight > best.weight then Some desire 
      else Some best
  ) None p_ship.desired_moves)
  with
  | None -> 0.
  | Some desire -> desire.weight
;;

let desire_untaken state desire =
  let row, col = desire.destination in
  (List.length state.persist.intent_map.(row).(col)) = 0
;;

let maximum_untaken_desire state p_ship =
  match (List.fold_left (fun acc desire ->
    if desire_untaken state desire then (
      match acc with
      | None -> Some desire
      | Some best -> 
        if desire.weight > best.weight then Some desire 
        else Some best
    )
    else acc
  ) None p_ship.desired_moves)
  with
  | None -> 0.
  | Some desire -> desire.weight
;;

(*
let rec finalise_ship_moves state commands ship =
  let filtered = (List.filter (fun desire -> desire_untaken state desire) ship.desired_moves) in
  Debug.debug (Printf.sprintf "Deciding between desires: %s\n" (Debug.string_of_desire_list filtered));
  (*List.iter (fun des -> Debug.debug (Printf.sprintf "Filtered desire: %s\n" (Debug.string_of_desire des))) filtered;*)
  let desires = List.sort (fun des1 des2 -> int_of_float (100000. *. (des2.weight -. des1.weight))) filtered in
  (*List.iter (fun des -> Debug.debug (Printf.sprintf "Sorted desire: %s\n" (Debug.string_of_desire des))) desires;*)
  match desires with
  | desire :: tail ->
    let drow, dcol = desire.destination in
    if (List.length state.persist.intent_map.(drow).(dcol)) > 0 then (
      ()
    )
    else (
      let row, col = desire.destination in
      begin match finalise_cell_move state (state.persist.desire_map.(row).(col)) with
      | Some desire' ->
        if (desire'.p_ship.entity.id != ship.entity.id) then (
          finalise_ship_moves state commands desire'.p_ship
        )
        else (
          Debug.debug (Printf.sprintf "Finalising ships_first desire: %s\n" (Debug.string_of_desire desire'));
          Debug.debug (Printf.sprintf "Other desires: %s\n" (Debug.string_of_desire_list tail));
          desire'.p_ship.move_decided <- true;
          commands := desire'.command :: !commands
        )
      | None -> 
        Debug.debug (Printf.sprintf "Warning! finalise_cell_move at %d, %d failed: %s\n" row col (Debug.string_of_desire desire));
        () (* continue := false *)
      end
    )
  | [] -> ()
;;

let finalise_ships_moves state =
  let continue = ref true in
  let commands = ref [] in
  while !continue do (
    let ships' = Game.my_p_ships_move_not_decided state in
    if ships' = [] then continue := false
    else (
      let ships = List.sort (fun ship1 ship2 ->
        let max1 = maximum_untaken_desire state ship1 in
        let max2 = maximum_untaken_desire state ship2 in
        (int_of_float (max2 -. max1))
      ) ships' in
      let ship = List.nth ships 0 in
      finalise_ship_moves state commands ship
    )
  )
  done;
  !commands
;;
*)

let finalise_ships_moves state =
  let ships' = Game.my_p_ships_move_not_decided state in
  let ships = List.sort (fun ship1 ship2 ->
    let max1 = maximum_untaken_desire state ship1 in
    let max2 = maximum_untaken_desire state ship2 in
    compare_float_desc max1 max2
    (*(int_of_float (max2 -. max1))*)
  ) ships' in
  List.fold_left (fun acc ship ->
    let filtered = (List.filter (fun desire -> desire_untaken state desire) ship.desired_moves) in
    (*Debug.debug (Printf.sprintf "Deciding between desires: %s\n" (Debug.string_of_desire_list filtered));*)
    (*List.iter (fun des -> Debug.debug (Printf.sprintf "Filtered desire: %s\n" (Debug.string_of_desire des))) filtered;*)
    let desires = List.sort (fun des1 des2 -> compare_float_desc des1.weight des2.weight) filtered in
    (*Debug.debug (Printf.sprintf "Deciding between sorted desires: %s\n" (Debug.string_of_desire_list desires));*)
    (*List.iter (fun des -> Debug.debug (Printf.sprintf "Sorted desire: %s\n" (Debug.string_of_desire des))) desires;*)
    match desires with
    | desire :: tail ->
      let drow, dcol = desire.destination in
      if (List.length state.persist.intent_map.(drow).(dcol)) > 0 then (
        acc
      )
      else (
        let row, col = desire.destination in
        begin match finalise_cell_move state (state.persist.desire_map.(row).(col)) with
        | Some desire' ->
          (*Debug.debug (Printf.sprintf "Finalising ships_first desire: %s\n" (Debug.string_of_desire desire'));*)
          (*Debug.debug (Printf.sprintf "Other desires: %s\n" (Debug.string_of_desire_list tail));*)
          desire'.p_ship.move_decided <- true;
          desire'.command :: acc
        | None -> 
          (*Debug.debug (Printf.sprintf "Warning! finalise_cell_move at %d, %d failed: %s\n" row col (Debug.string_of_desire desire));*)
          acc
        end
      )
    | [] -> acc
  ) [] ships
;;

let finalise_cell_moves state =
  let cells' = get_undecided_desire_cells state in
  (*Debug.debug (Printf.sprintf "Number of desire cells = %d\n" (List.length cells'));*)
  let cells = List.rev (List.sort (fun a b ->
    (compare_float_desc (sum_desire_weight a) (sum_desire_weight b) (*int_of_float ((sum_desire_weight b) -. (sum_desire_weight a))*))
  ) cells') in
  List.fold_left (fun acc desires ->
    match finalise_cell_move state desires with
    | Some desire -> 
      (*Debug.debug (Printf.sprintf "Finalising cells_second desire: %s\n" (Debug.string_of_desire desire));*)
      desire.p_ship.move_decided <- true;
      desire.command :: acc
    | None -> acc
  ) [] cells
;;

let finalise_moves_ships_first state =
  let rec finalise_all_ship_moves state =
    match finalise_ships_moves state with
    | [] -> []
    | moves -> moves @ finalise_all_ship_moves state
  in
  let ship_moves = finalise_all_ship_moves state in
  let cell_moves = finalise_cell_moves state in
  amend_suicide_commands state (ship_moves @ cell_moves)
    
;;

let finalise_moves state =
  let cells' = get_desire_cells state.persist.desire_map in
  (*Debug.debug (Printf.sprintf "Number of desire cells = %d\n" (List.length cells'));*)
  let cells = List.rev (List.sort (fun a b ->
    compare_float_asc (sum_desire_weight a) (sum_desire_weight b)
    (*int_of_float ((sum_desire_weight a) -. (sum_desire_weight b))*)
  ) cells') in
  let desired_commands =
    List.fold_left (fun acc desires ->
      match finalise_cell_move state desires with
      | Some desire -> 
        desire.p_ship.move_decided <- true;
        desire.command :: acc
      | None -> acc
    ) [] cells
  in
  amend_suicide_commands state (desired_commands)
;;

let moves_not_finalised state =
  List.exists (fun p_ship ->
    not p_ship.move_decided
  ) (Game.my_p_ships state)
;;

let shipyard_unoccupied state player =
  let shipyard = Game.get_shipyard state player in
  let row, col = shipyard.position in
  (List.length state.persist.intent_map.(row).(col)) = 0
;;

let save_grid_picture tag grid threshold =
  let colour_map = Array.map (Array.map (fun cell ->
    if cell >= threshold then (0, 0, cell) 
    else (cell / 3, cell / 3, cell / 3)
  )) grid in
  let filename = Printf.sprintf "image_%s_%05d.pnm" tag !file_count in
  file_count := !file_count + 1;
  Render.write_pnm filename colour_map
;;

let descending_int_comparison i1 i2 = i2 - i1;;

let find_sum_threshold grid target =
  let cells = flatten_2d_array grid in
  Array.stable_sort descending_int_comparison cells;
  let acc = ref 0 in
  let threshold_count = ref 0 in
  try
    let cells' = Array.to_list cells in
    let result = List.find (fun cell ->
      acc := !acc + cell;
      threshold_count := !threshold_count + 1;
      !acc >= target
    ) cells' in
    (*Debug.debug (Printf.sprintf "Threshold reached after %d of %d cells\n" !threshold_count (Array.length cells));*)
    result
  with Not_found -> target
;;

(* Find the threshold value within which the top n/1 cells lie *)
let grid_top_nratio_threshold grid n =
  let total = sum_grid grid in
  let target = int_of_float (ceil (float_of_int total *. n)) in
  find_sum_threshold grid target
;;

let grid_top_n_cells_thresholds grid nlist =
  let cells = flatten_2d_array grid in
  Array.stable_sort descending_int_comparison cells;
  List.map (fun n -> cells.(n)) nlist
;;

let time_fn f arg =
  let time_start = Unix.gettimeofday() in
  let result = f arg in
  let time_finish = Unix.gettimeofday() in
  let time_taken = time_finish -. time_start in
  Debug.debug(Printf.sprintf "Function took %f milliseconds\n" (time_taken *. 1000.));
  result;
;;

let locations_matching grid condition =
  let locs = ref [] in
  Array.iteri (fun ir row ->
    Array.iteri (fun ic cell ->
      if condition cell then locs := (ir, ic) :: !locs
    ) row
  ) grid;
  !locs
;;

let locations_atleast_value grid value =
  locations_matching grid (fun v -> v >= value)
;;

let crossed_threshold_desc grid threshold (r1, c1) (r2, c2) =
  grid.(r1).(c1) >= threshold && grid.(r2).(c2) < threshold
;;

let dist_init dist l q =
  List.iter (fun (row, col) -> 
    Search.simple_init q (row, col); 
    dist.(row).(col) <- 0
  ) l
;;

let locations_atleast_value_within grid value rect =
  let locs = ref [] in
  try
    let grid_rows, grid_cols = Array.length grid, Array.length grid.(0) in
    let srow, scol, rows, cols = rect in
    for urow = srow to srow + rows - 1 do
      for ucol = scol to scol + cols - 1 do
        let row, col = Game.wrap_grid_position (grid_rows, grid_cols) (urow, ucol) in
        if grid.(row).(col) >= value then
          locs := (row, col) :: !locs
      done
    done;
    !locs
  with _ -> !locs
;;

let sum_and_count_locations_atleast_value_within_bfs_dist state grid value position distance =
  let sum = ref 0 in
  let num = ref 0 in
  let dist = Array.map (Array.map (fun _ -> max_int)) state.map in
  let should_terminate (row, col) =
    dist.(row).(col) > state.const.inspiration_radius
  in
  let visit = fun (row, col) next q -> 
    Search.simple_bfs_visit dist (row, col) next q;
    if (state.map.(row).(col) >= value) then (
      sum := !sum + state.map.(row).(col);
      num := !num + 1;
    )
  in
  let get_next = Search.grid_get_next dist in
  let init = (dist_init dist [position]) in
  Search.short_bfs init visit get_next should_terminate;
  !sum, !num
;;

let unsquashed_rect row col factor =
  row * factor, col * factor, factor, factor
;;

let inspired_map state perspective =
  Array.mapi (fun ir row -> Array.mapi (fun ic cell ->
    let count = List.fold_left (fun acc p_ship ->
      if p_ship.entity.owner = perspective then acc else acc + 1
    ) 0 state.persist.ships_near.(ir).(ic) in
    if count > 1 then cell * 3 else cell
  ) row) state.map
;;

let make_inspired_condensed_map state =
  let grid = inspired_map state state.my_id in
  squashed_int_grid grid state.persist.condense_factor state.persist.condense_factor
;;

let init_searches state =
  let factor = state.persist.condense_factor in
  let condensed_map = squashed_int_grid state.map factor factor in
  state.persist.searches <-
    Array.mapi (fun ir row -> Array.mapi (fun ic cell ->
      Array.map (Array.map (fun _ -> max_int)) state.map
    ) row) condensed_map
;;

let reset_dist arr =
  Array.iteri (fun ir row -> Array.iteri (fun ic cell ->
    arr.(ir).(ic) <- max_int;
  ) row) arr
;;

let update_area_searches state =
  let map_size = state.height * state.width in
  (*Debug.debug (Printf.sprintf "Time grid_top_n_cells for %d x %d map\n" state.height state.width);*)
  let lastship_count = min (List.length state.ships) (map_size - 1) in
  let top_thresholds = time_fn (grid_top_n_cells_thresholds state.map) [map_size / 16; map_size / 8; map_size / 4; map_size / 2; lastship_count] in
  let top_thresholds' = List.mapi (fun i t -> if i = 3 then min 99 t else t) top_thresholds in
  state.persist.percentile_thresholds <- top_thresholds';
  (* this is the median halite value *)
  let median_threshold = List.nth top_thresholds 3 in
  let factor = 4 in
  let condensed_map = squashed_int_grid state.map factor factor in
  debug_int_grid (normalise_int_grid condensed_map 254);
  (* This is the cell value above which half the halite on the map is found *)
  let threshold = grid_top_nratio_threshold condensed_map 0.5 in
  state.persist.half_sum_threshold <- threshold;
  (*Debug.debug (Printf.sprintf "Threshold = %d (%x in hex)\n" threshold threshold);*)
  Array.iteri (fun ir row -> Array.iteri (fun ic cell ->
      let superset_starting_points = locations_atleast_value_within state.map  median_threshold (unsquashed_rect ir ic factor) in
      let highest_starting_value = List.fold_left (fun acc (row, col) ->
        if state.map.(row).(col) > acc then state.map.(row).(col) else acc
      ) 0 superset_starting_points in
      let starting_points' = List.filter (fun (row, col) ->
          state.map.(row).(col) >= highest_starting_value / 2
          && (highest_starting_value < median_threshold
            || state.map.(row).(col) >= median_threshold)
      ) superset_starting_points in
      let starting_points = if List.length starting_points' > 0 then starting_points' else superset_starting_points in
      reset_dist state.persist.searches.(ir).(ic);
      let dist = state.persist.searches.(ir).(ic) in
      let visit = Search.simple_bfs_visit dist in
      let get_next = Search.grid_get_next dist in
      let should_terminate (row, col) =
        dist.(row).(col) > 36
      in
      Search.short_bfs (dist_init dist starting_points) visit get_next should_terminate;
  ) row) condensed_map;
  state.persist.condensed_map <- condensed_map;
  (*condensed_map, searches*)
;;

let dist_owned_init dist owned starting_points q =
  List.iter (fun ((row, col), owner) -> 
    Search.simple_init q (row, col); 
    dist.(row).(col) <- 0;
    owned.(row).(col) <- owner;
  ) starting_points
;;

let voronoi_search state owned_starting_points =
  let dist = Array.map (Array.map (fun _ -> max_int)) state.map in
  let owned = Array.map (Array.map (fun _ -> -1)) state.map in
  let visit = Search.voronoi_visit dist owned in
  let get_next = Search.grid_get_next dist in
  Search.bfs (dist_owned_init dist owned owned_starting_points) visit get_next;
  dist, owned
;;

let weight_halite_low_good state halite_grid grid =
  Array.mapi (fun ir row -> Array.mapi (fun ic cell ->
    cell * state.const.max_energy +
      ((min state.const.max_energy halite_grid.(ir).(ic)) / 2)
  ) row) grid
;;

let dist_search state starting_points =
  let dist = Array.map (Array.map (fun _ -> max_int)) state.map in
  let visit = Search.simple_bfs_visit dist in
  let get_next = Search.grid_get_next dist in
  Search.bfs (dist_init dist starting_points) visit get_next;
  dist
;;

let obstacle_search state obstacles starting_points =
  let dist = Array.map (Array.map (fun _ -> max_int)) state.map in
  let visit = Search.simple_bfs_visit dist in
  let get_next = Search.obstacle_grid_get_next obstacles dist in
  Search.bfs (dist_init dist starting_points) visit get_next;
  dist
;;

let all_dropoff_searches state =
  Array.mapi (fun player_id _ ->
    let dropoffs = Game.dropoff_positions_of_player state player_id in
    let shipyard = (Game.get_shipyard state player_id).position in
    dist_search state (shipyard :: dropoffs)
  ) state.energy
;;

let update_danger_maps state =
  state.persist.danger_maps <- Array.mapi (fun player_id _ ->
    let starting_points = List.fold_left (fun acc ship ->
      if ship.owner != player_id then ship.position :: acc
      else acc
    ) [] state.ships in
    dist_search state starting_points
  ) state.energy
;;

let high_halite_tiles state =
  let positions = ref [] in
  Array.iteri (fun ir row -> Array.iteri (fun ic halite ->
    if halite > state.persist.magic.endgame_high_halite then 
      positions := (ir, ic) :: !positions
  ) row) state.map;
  !positions
;;

let high_halite_enemy_ship_positions state =
  let positions = ref [] in
  List.iter (fun p_ship ->
    if p_ship.entity.owner != state.my_id
    && (p_ship.entity.halite > state.persist.magic.endgame_high_halite) then
      positions := p_ship.entity.position :: !positions
  ) state.persist.p_ships;
  !positions
;;

let find_likely_return_destinations state p_ship =
  if p_ship.prev_position = p_ship.entity.position then [] else (
    let search = state.persist.all_dropoff_searches.(p_ship.entity.owner) in
    let adjacent = Search.get_adjacent search p_ship.entity.position in
    let srow, scol = p_ship.entity.position in
    List.filter (fun (row, col) ->
      search.(row).(col) <= search.(srow).(scol)
    ) adjacent
  )
;;

let high_halite_enemy_ship_positions_and_destinations state =
  let positions = ref [] in
  List.iter (fun p_ship ->
    if p_ship.entity.owner != state.my_id
    && (p_ship.entity.halite > state.persist.magic.endgame_high_halite) then (
      let likely_destinations = find_likely_return_destinations state p_ship in
      positions := p_ship.entity.position :: likely_destinations @ !positions
      )
  ) state.persist.p_ships;
  !positions
;;

let ship_targets state =
  let targets = ref [] in
  List.iter (fun p_ship ->
    if p_ship.entity.owner != state.my_id
    && (p_ship.entity.halite > state.persist.magic.endgame_high_halite) then
      let harvest = 
        Harvest (p_ship, find_likely_return_destinations state p_ship) 
      in
      targets := harvest :: !targets
  ) state.persist.p_ships;
  !targets
;;

let enemy_base_entry_points state =
  let positions = ref [] in
  Array.iteri (fun ir row -> Array.iteri (fun ic _ ->
    if List.exists (fun entity ->
      entity.owner != state.my_id
      && (Game.calculate_distance state (ir, ic) entity.position) = 1
    ) ((state.dropoffs) @ (state.shipyards))
  then
    positions := (ir, ic) :: !positions
  ) row) state.map;
  !positions
;;

let create_crash_search state include_base_entry =
  let starting_points = 
    (high_halite_tiles state) 
    @ (high_halite_enemy_ship_positions_and_destinations state)
    @ if include_base_entry then 
      enemy_base_entry_points state
    else 
      [] 
  in
  let filtered = List.filter (fun position -> not (List.exists (fun entity ->
    entity.owner != state.my_id
    && (entity.position = position)
  ) (state.dropoffs @ state.shipyards))) starting_points in
  dist_search state filtered
;;

let create_all_enemy_search state =
  let starting_points = 
    List.fold_left (fun acc ship ->
      if ship.owner != state.my_id then ship.position :: acc else acc
    ) [] state.ships
  in
  dist_search state starting_points
;;

let carried_halite state player =
  List.fold_left (fun acc ship ->
    if ship.owner = player then acc + ship.halite else acc
  ) 0 state.ships
;;

let endgame_score_guess state =
  Array.mapi (fun player_index stored ->
    stored + carried_halite state player_index
  ) state.energy
;;

let players_threatening_my_victory state =
  let balance = endgame_score_guess state in
  Array.fold_left (fun (player, acc) projected ->
    if player = state.my_id then (player + 1, acc)
    else if (state.energy.(player) <= balance.(state.my_id))
    && (projected >= balance.(state.my_id)) then (
      (player + 1, player :: acc)
    )
    else (
      (player + 1, acc)
    )
  ) (0, []) balance
;;

(*
let dropoff_search state include_shipyard =
  let starting_points = 
    if include_shipyard then 
      Game.my_shipyard_position state :: Game.my_dropoff_positions state
    else Game.my_dropoff_positions state
  in
  let dist = dist_search state starting_points in
  dist, weight_halite_low_good state state.map dist;
;;
*)

let safe_dropoff_search state include_shipyard =
  let starting_points = 
    if include_shipyard then 
      Game.my_shipyard_position state :: Game.my_dropoff_positions state
    else Game.my_dropoff_positions state
  in
  let obstacles = state.persist.obstacles in
  let dist = obstacle_search state obstacles starting_points in
  dist
;;

let ship_count_ratio state =
  let ship_count = Array.make state.num_players 0 in
  List.iter (fun ship ->
    ship_count.(ship.owner) <- ship_count.(ship.owner) + 1
  ) state.ships;
  let my_ship_count = ship_count.(state.my_id) in
  let _, opponent_ship_count = Array.fold_left (fun (i, acc) v ->
    (i + 1), if i = state.my_id then acc else max v acc
  ) (0, 0) ship_count in
  my_ship_count, opponent_ship_count
;;

let build_ship_count_threshold state =
  if state.turn < state.width * 2 then 8
  else if state.turn < state.width * 3 then 4
  else if state.turn < state.width * 4 then 2
  else 0
;;

let halite_count_ratio state =
  let my_halite = state.energy.(state.my_id) in
  let _, opponent_max_halite = Array.fold_left (fun (i, acc) v ->
    i + 1, if i = state.my_id then acc else max v acc
  ) (0, 0) state.energy in
  my_halite, opponent_max_halite
;;

let mining_turns_to_build_new_ship state =
  (*Debug.debug "old_mining_turns_to_build_new_ship \n";*)
  let rec count_tiles_to target per_tile =
    if per_tile * 4 / 5 < 1 then max_int / 2
    else if per_tile * 4 / 5 > target then 8
    else 8 + (count_tiles_to (target - (per_tile * 4 / 5)) per_tile)
  in
  let median = List.nth state.persist.percentile_thresholds 3 in
  let optimistic = List.nth state.persist.percentile_thresholds 1 in
  (*Debug.debug (Printf.sprintf "median = %d, optimistic = %d\n" median optimistic);*)
  let median_turns = count_tiles_to state.const.new_entity_energy_cost median in
  let optimistic_turns = count_tiles_to state.const.new_entity_energy_cost optimistic in
  max 1 ((median_turns + optimistic_turns + optimistic_turns) / 3)
;;

let average_collected_per_turn cycles =
  let averages = List.map (fun cycle ->
    float_of_int cycle.amount_returned /. (float_of_int (max 1 (cycle.turn_completed - cycle.turn_begun)))
  ) cycles in
  let s = List.fold_left (fun acc v -> acc +. v) 0. averages in
  s /. float_of_int (List.length averages)
;;

let old_should_build_more_ships state =
  (state.num_players = 2 && (List.length (Game.my_ships state) < (List.length (Game.not_my_ships state) - 3)) && (
    let mining_turns = mining_turns_to_build_new_ship state in
    let payoff_time = (state.width + mining_turns) * 9 / 10  in
    payoff_time < Game.turns_remaining state
  ))
  || (
    (List.length (Game.my_ships state) <= state.persist.magic.min_ships_before_dropoff)
    && ((Game.largest_single_not_my_ships state) > List.length (Game.my_ships state))
    && (state.turn < state.const.max_turns * 2 / 5)
  )
  || (
    let mining_turns = mining_turns_to_build_new_ship state in
    let payoff_time = Magic.mul_int (state.width + mining_turns) state.persist.magic.ship_build_payoff_mul in
    (*Debug.debug (Printf.sprintf "should_build : mining_turns = %d, payoff_time = %d\n" mining_turns payoff_time);*)
    payoff_time < Game.turns_remaining state
  )
;;

let new_should_build_more_ships state =
  (*Debug.debug "new_mining_turns_to_build_new_ship\n";*)
  let recent_cycles = List.map 
    (fun p_ship ->
      match p_ship.last_collection with
      | None -> failwith "new_mining_turns_to_build_new_ship self-destructed"
      | Some result -> result
    ) 
    (List.filter 
      (fun p_ship -> p_ship.last_collection != None) state.persist.p_ships)
  in
  if List.length recent_cycles = 0 then None
  else (
    let ship_num_mul = if state.num_players = 4 then 1. else 1. in
    let ship_num = int_of_float (float_of_int (List.length state.ships + 3) *. ship_num_mul) in
    let average = average_collected_per_turn recent_cycles in
    let total_per_turn = float_of_int ship_num *. average in
    let total_available = 0.75 *. float_of_int state.persist.total_halite_available in
    let max_good_mining_turns = int_of_float (total_available /. total_per_turn) in
    let cost = float_of_int state.const.new_entity_energy_cost *. 1.1 in
    let turns_remaining = Game.turns_remaining state in
    let conservative_turns = (min max_good_mining_turns turns_remaining) * 7 / 9 in
    (*let max_individual_earnings = (min total_available /. (float_of_int ship_num)) average *. conservative_turns in*)
    Some (float_of_int conservative_turns *. average > cost
     (*&& (float_of_int turns_remaining *. average > cost) *)
      && (int_of_float total_available / ship_num > int_of_float cost))
  )
;;

let should_build_more_ships state =
  old_should_build_more_ships state
(*
  match new_should_build_more_ships state with
  | None -> old_should_build_more_ships state
  | Some result -> result || old_should_build_more_ships state
*)
;;

let mark_ship_near state dist ships_near p_ship loc next q =
  let p_row, p_col = loc in
  if dist.(p_row).(p_col) <= state.const.inspiration_radius then (
    ships_near.(p_row).(p_col) <- p_ship :: ships_near.(p_row).(p_col)
  );
  List.iter (fun (n_row, n_col) ->
    if dist.(n_row).(n_col) = max_int then (
      dist.(n_row).(n_col) <- dist.(p_row).(p_col) + 1;
      Queue.push (n_row, n_col) q;
    )
  ) next
;;

let update_ships_near state =
  (*Debug.debug "update_ships_near\n";*)
  state.persist.ships_near <- Array.map (Array.map (fun _ -> [])) state.map;
  let dist = Array.map (Array.map (fun _ -> max_int)) state.map in
  let mark_near_ship p_ship =
    let starting_points = [p_ship.entity.position] in
    let should_terminate (row, col) =
      dist.(row).(col) > state.const.inspiration_radius
    in
    let visit = mark_ship_near state dist state.persist.ships_near p_ship in
    (*let visit = Search.avoid_visit dist (List.map (fun base -> base.position) (Game.my_bases state)) in*)
    let get_next = Search.grid_get_next dist in
    Search.short_bfs (dist_init dist starting_points) visit get_next should_terminate;
    reset_dist dist;
  in
  List.iter mark_near_ship state.persist.p_ships
;;

let locations_matching_within grid rect f =
  let locs = ref [] in
  try
    let grid_rows, grid_cols = Array.length grid, Array.length grid.(0) in
    let srow, scol, rows, cols = rect in
    for urow = srow to srow + rows - 1 do
      for ucol = scol to scol + cols - 1 do
        let row, col = Game.wrap_grid_position (grid_rows, grid_cols) (urow, ucol) in
        if f (row, col) then
          locs := (row, col) :: !locs
      done
    done;
    !locs
  with _ -> !locs
;;

let condensed_cells_matching state (row, col) f =
  locations_matching_within state.map (unsquashed_rect row col state.persist.condense_factor) f
;;

let update_condensed_areas_under_threat state =
  Array.iteri (fun ir row -> Array.iteri (fun ic cell ->
    let under_threat = condensed_cells_matching state (ir, ic) 
      (fun (row, col) ->
        List.exists (fun p_ship ->
          p_ship.entity.owner != state.my_id
          &&
          Game.calculate_distance state p_ship.entity.position (row, col) < 2
        ) state.persist.ships_near.(row).(col)
    ) in
    state.persist.condensed_area_under_threat.(ir).(ic) <- 
      List.length under_threat > 0
  ) row) state.persist.condensed_area_under_threat
;;

let update_cells_under_threat state =
  Array.iteri (fun ir row ->
    Array.iteri (fun ic _ ->
      state.persist.under_threat.(ir).(ic) <- false
  ) row) state.persist.under_threat;

  List.iter (fun p_ship ->
    if p_ship.entity.owner != state.my_id then (
      let cells = p_ship.entity.position :: Search.get_adjacent state.map p_ship.entity.position in
      List.iter (fun (row, col) ->
        state.persist.under_threat.(row).(col) <- true
      ) cells
    )
  ) state.persist.p_ships
;;

let nearship_balance state (row, col) =
  let ships_near = state.persist.ships_near.(row).(col) in
  List.fold_left (fun (myc, enc) near_ship ->
    if near_ship.entity.owner = state.my_id then (myc + 1, enc)
    else (myc, enc + 1)
  ) (0, 0) ships_near
;;

let bravery state p_ship =
  let my_count, enemy_count = nearship_balance state p_ship.entity.position in
  let ship_bravery = float_of_int my_count /. (float_of_int (my_count + enemy_count)) in
  (*Debug.debug (Printf.sprintf "bravery = %f\n" ship_bravery);*)
  ship_bravery *. ship_bravery
;;

let normalise_float min_v max_v v' =
  let range = max_v -. min_v in
  let v = v' -. min_v in
  let result = v /. range in
  (*Debug.debug (Printf.sprintf "normalise range %f to %f = %f, v = %f, result = %f\n" max_v min_v range v' result);*)
  result
;;

(*
let normalise_pos_float_nozero min_v max_v v =
  let result = v /. max_v in
  result
;;

let normalise_weighted_directions weighted =
  let float_extremities l =
    List.fold_left (fun (prev_min, prev_max) (_, v) ->
      (max v prev_min), (min v prev_max)
    ) (max_float, min_float) l
  in
  let min_v, max_v = float_extremities weighted in
  List.map (fun (dir, v) ->
    dir, normalise_pos_float_nozero min_v max_v v
  ) weighted
;;
*)

let add_primary_plus_weighted_desires state p_ship eval primary primary_ratio mul =
  (*Debug.debug "add_primary_plus_weighted_desires \n";*)
  let weighted = List.map (fun dir ->
    (*if dir = primary then (dir, ref 0.)*)
    (*else ( *)
      let target = Game.directional_offset_position state p_ship.entity.position dir in
      let score = eval target in 
      (dir, ref score)
    (* ) *)
  ) Game.all_dirs in
  List.iter (fun (dir, score) ->
    if dir = primary then 
      score := primary_ratio *. 
        (List.fold_left (fun acc (_, s) ->
          max !s acc
        ) min_float weighted)
  ) weighted;
  let weighted' = List.map (fun (dir, r) -> dir, !r) weighted in
  (*let normalised = normalise_weighted_directions weighted' in*) (* apparently not! *)
  add_weighted_desires state p_ship weighted' mul
;;

let normalise_p_ship_desires p_ship =
  let min_v, max_v = List.fold_left (fun (acc_max, acc_min) desire ->
    min desire.weight acc_max, max desire.weight acc_min
  ) (max_float, min_float) p_ship.desired_moves in
  List.iter (fun desire ->
    desire.weight <- normalise_float min_v max_v desire.weight
  ) p_ship.desired_moves;
  let sum = List.fold_left (fun acc desire -> acc +. desire.weight) 0.0 p_ship.desired_moves in
  List.iter (fun desire ->
    desire.weight <- desire.weight /. (max 0.1 sum)
  ) p_ship.desired_moves
;;

let squash_p_ship_desires p_ship =
  let _, max_v = List.fold_left (fun (acc_max, acc_min) desire ->
    min desire.weight acc_max, max desire.weight acc_min
  ) (max_float, min_float) p_ship.desired_moves in
  let sum = List.fold_left (fun acc desire -> acc +. desire.weight) 0.0 p_ship.desired_moves in
  let squash_amount = sum /. max_v in
  List.iter (fun desire ->
    desire.weight <- desire.weight /. (max 0.01 squash_amount)
  ) p_ship.desired_moves
;;

let multiply_p_ship_desires p_ship amount =
  List.iter (fun desire ->
    desire.weight <- desire.weight *. amount
  ) p_ship.desired_moves
;;

let normalise_all_desires state =
  List.iter normalise_p_ship_desires (Game.my_p_ships state)
;;

let squash_all_desires state =
  List.iter squash_p_ship_desires (Game.my_p_ships state)
;;

let stretch_p_ship_desires state p_ship =
  let stretch_amount = 1. +. ((float_of_int p_ship.entity.halite) /. (float_of_int state.const.max_energy) /. 4.) in
  (*let stretch_amount = 1. in*)
  multiply_p_ship_desires p_ship stretch_amount
;;

let squash_then_stretch_all_desires state =
  List.iter squash_p_ship_desires (Game.my_p_ships state);
  List.iter (stretch_p_ship_desires state) (Game.my_p_ships state);
;;

let ship_loss_penalty state =
  let best_enemy_ship_count =
    Game.largest_single_not_my_ships state
  in
  let player_ship_count = List.length (Game.ships_of_player state state.my_id) in
  let d = player_ship_count - best_enemy_ship_count in
  if (d < 0) then (
    (*let own_ratio = float_of_int (-d) /. float_of_int player_ship_count in*)
    (*16384. *. own_ratio*)
    2.
(*    max 0. (0.05 -. (own_ratio *. float_of_int (state.num_players - 1))) *)
  )
  else if (d = 0) then (
    if state.num_players = 2 then (
      1.
    )
    else (
      2.
    )
  )
  else (
    (*let own_ratio = float_of_int d /. float_of_int player_ship_count in*)
    (*max 0. (0.1 -. (own_ratio *. float_of_int (state.num_players - 1)))*)
    1.
  )
;;


let inspired_danger_score state p_ship position =
  let dangers = Risk.crash_dangers state p_ship position in
  let danger_sum =
    List.fold_left (fun acc (e_ship, score, _, didnot) ->
      let score' = max 0.25 score in
      let add =
        if ((p_ship.crash_risk_wait > e_ship.crash_risk_wait) || (e_ship.crash_risk_wait > state.persist.risk_turns)) && (e_ship.entity.position != position) then 0.1
        else if didnot < 8 then 1.
        else if e_ship.entity.position = position then
          max 0.5 (score *. 4.)
        else score'
      in
      (*Debug.debug ("add " ^ (string_of_float score));*)
      acc +. add
    ) 0. dangers
  in
  (*Debug.debug (Printf.sprintf "num dangers = %d, danger_sum = %f\n" (List.length dangers) danger_sum);*)
  let mul = ship_loss_penalty state in
  max 0.0000001 (min 1. (1. -. (min 1. (danger_sum *. mul))))
;;

let return_search state =
  let starting_points =
    if (List.length (Game.my_dropoff_positions state) < 2) then
      Game.my_shipyard_position state :: Game.my_dropoff_positions state
    else Game.my_dropoff_positions state
  in
  let dist = dist_search state starting_points in
  dist
;;

