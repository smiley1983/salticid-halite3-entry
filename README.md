Salticid Halite 3 contest entry
-------------------------------

This was the code I submitted for my final entry into the Halite 3 programming contest. In the last week before the deadline, the better versions generally achieved a rank above 50, but rarely above 40.

The code is a bit of a mess. I don't recommend reading it. If you want to, start with MyBot.ml and follow any functions which interest you from there. There's a lot of dead code, so reading through whole files will not give you much idea of how anything works.

The fine details really affect the final performance. Some "improvements" I tried - which really seemed to work locally - caused a drop from around rank 40 to around rank 80. There were a few things I was happy to be able to include in my entry:

 - Assignments are set for all ships before any moves are calculated, and it spends some time attempting to iteratively improve on these assignments (e.g spreading ships out so they don't all target the same region).

 - An attempt is made to iteratively improve inspiration-relevant orders to maximise my own inspiration while minimising that of others. This could have used a lot more testing and troubleshooting, but it definitely improved performance.

 - Risk-taking behaviour of opponents is tracked and used to improve the chance of occupying threatened tiles without crashing. I think this could have been made to work better than it does, but I was happy that it helped at all.

There were many things I ran out of time to implement, or left half-implemented and disabled. It seems I could probably have benefited from slowing down on the new features, tidying up the code, and consolidating the core logic to eliminate specific erroneous behaviour. I kept putting those things off, enjoying the opportunity to be undisciplined and begin implementing every fun and exciting idea which seemed promising at the time - safe in the knowledge that I never have to touch this code again. I don't think this is the correct practice, but it's what I did, and I have enjoyed the contest immensely. :)

Thank you to all contestents and to everyone who contributed to running this excellent programming competition!

The website can be found at halite.io

