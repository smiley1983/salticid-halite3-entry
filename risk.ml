open Type;;

let highest_risk risk_array =
  Array.fold_left (fun (best_did, best_didnot) (did, didnot) ->
    if did > best_did && (best_didnot = 0) then did, didnot
    else (
      let best_score = float_of_int best_did /. float_of_int best_didnot in
      let score = float_of_int did /. float_of_int didnot in
      if score > best_score then did, didnot else best_did, best_didnot
    )
  ) (0, 1000) risk_array
;;

let friendly_occupied state position =
  List.exists (fun ship -> ship.position = position) state.ships
;;

let crash_dangers state p_ship (row, col) =
  let risk_type =
    (
    if friendly_occupied state (row, col) then
      state.persist.crash_risk_occupied
    else state.persist.crash_risk_unoccupied
    )
  in
  let dangers = List.fold_left (fun acc e_ship ->
    (* conditional is a safeguard, should not be needed with could_inspire *)
    if p_ship.entity.owner != e_ship.entity.owner then (
      let distance = Game.calculate_distance state (row, col) e_ship.entity.position in
      if distance < 2 then e_ship :: acc
      else acc
    )
    else acc
  ) [] p_ship.could_inspire in

  (*
  let owners = List.fold_left (fun acc danger ->
    if not List.exists (fun owner ->
      owner = danger.entity.owner
    ) acc then danger.entity.owner :: acc else acc
  ) [] dangers in

  List.length owners > 1
  || (
  *)
    List.map (fun e_ship ->
      let risk_array = risk_type.(e_ship.entity.owner) in
      let this_did, this_didnot = risk_array.(min state.persist.risk_turns e_ship.crash_risk_wait) in

      if this_didnot = 0 then (
        e_ship, 1., 1., 0 (* FIXME kind of muddling through here, check this later... *)
      )
      else (
        let highest_did, highest_didnot = highest_risk risk_array in
        let highest = float_of_int highest_did /. float_of_int highest_didnot in
        let this_score = float_of_int this_did /. float_of_int this_didnot in
        e_ship, this_score, highest, this_didnot
      )
    ) dangers
  (* ) *)
;;


let crash_likelihood_alert p_ship state (row, col) =
  let risk_type = 
    (
    if friendly_occupied state (row, col) then 
      state.persist.crash_risk_occupied
    else state.persist.crash_risk_unoccupied
    )
  in
  let dangers = List.fold_left (fun acc e_ship ->
    (* conditional is a safeguard, should not be needed with could_inspire *)
    if p_ship.entity.owner != e_ship.entity.owner then ( 
      let distance = Game.calculate_distance state e_ship.entity.position p_ship.entity.position in
      if distance < 3 then e_ship :: acc
      else acc
    )
    else acc
  ) [] p_ship.could_inspire in

    List.exists (fun e_ship ->
      e_ship.entity.position = (row, col)
    ) dangers
    ||
  (*
  let owners = List.fold_left (fun acc danger ->
    if not List.exists (fun owner ->
      owner = danger.entity.owner
    ) acc then danger.entity.owner :: acc else acc
  ) [] dangers in

  List.length owners > 1
  || (
  *)
    List.exists (fun e_ship ->
      let risk_array = risk_type.(e_ship.entity.owner) in
      let this_did, this_didnot = risk_array.(min state.persist.risk_turns e_ship.crash_risk_wait) in

      this_didnot = 0 
      || (
        let highest_did, highest_didnot = highest_risk risk_array in
        let highest = float_of_int highest_did /. float_of_int highest_didnot in
        let this_score = float_of_int this_did /. float_of_int this_didnot in
        this_score >= highest *. state.persist.magic.crash_warn_downplay (* FIXME magic *)
      )
    ) dangers
  (* ) *)
;;


