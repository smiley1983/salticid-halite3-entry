(* OCaml Starter for Halite III on Halite.io
   This code is public domain. There is no warranty. 
 *)

open Type;;

let out_chan = ref stderr;;
let flog_chan = ref stderr;;
let input_out_chan = ref stderr;;
let data_chan = ref stderr;;

let file_left_open = ref false;;

let setup_logging name tag =
  let filename = Printf.sprintf "%s_%s.log" name tag in
  out_chan := open_out filename;
  let input_filename = Printf.sprintf "%s_%s_input.log" name tag in
  input_out_chan := open_out input_filename;
  let flog_filename = Printf.sprintf "%s_%s.flog" name tag in
  flog_chan := open_out flog_filename;
  output_string !flog_chan "[\n";
  let data_filename = Printf.sprintf "%s_%s.data" name tag in
  data_chan := open_out data_filename;
  file_left_open := true
;;

let close_files () =
  if !file_left_open then (
    close_out !out_chan;
    output_string !flog_chan "\n]\n";
    close_out !flog_chan;
    close_out !input_out_chan;
    close_out !data_chan;
  )
;;

let log_input s =
  output_string !input_out_chan s; 
  flush !input_out_chan
;;

let debug s =
  output_string !out_chan s; 
  flush !out_chan
;;

let flog_colour turn row col r g b =
  output_string !flog_chan (Printf.sprintf "{\"t\":%d, \"x\":%d, \"y\":%d, \"color\":\"#%02x%02x%02x\"},\n" (turn - 1) col row r g b);
  flush !flog_chan
;;

let flog_msg turn row col s =
  output_string !flog_chan (Printf.sprintf "{\"t\":%d, \"x\":%d, \"y\":%d, \"msg\":\"%s\"},\n" (turn - 1) col row s);
  flush !flog_chan
;;

let info s = 
  output_string !out_chan s; 
  flush !out_chan
;;

let data s = 
  output_string !data_chan s; 
  flush !data_chan
;;

let error s = 
  output_string stderr ("ERROR: " ^ s ^ "\n"); 
  flush stderr
;;

let string_of_p_ship p_ship =
  let row, col = p_ship.entity.position in
  Printf.sprintf "Ship: owner = %d; id = %d; position = %d, %d; halite = %d; num desired moves = %d\n" p_ship.entity.owner p_ship.entity.id row col p_ship.entity.halite (List.length p_ship.desired_moves)
;;

let string_of_dir = function
| North -> "North"
| East -> "East"
| South -> "South"
| West -> "West"
| Still -> "Still"
;;

let string_of_command = function
  | Generate -> "g"
  | Construct ship_id -> "c " ^ (string_of_int ship_id) ^ " "
  | Move (ship_id, dir) -> 
    "m " ^ (string_of_int ship_id) ^ " " ^ begin match dir with
      | North -> "n"
      | East -> "e"
      | South -> "s"
      | West -> "w"
      | Still -> "o"
      end ^ " "
;;

let string_of_desire desire =
  let row, col = desire.destination in
  (Printf.sprintf "p_ship = %s; command = %s; destination = %d, %d; weight = %f\n" (string_of_p_ship desire.p_ship) (string_of_command desire.command) row col desire.weight);
;;

let string_of_desire_list l =
  List.fold_left (fun acc desire ->
    acc ^ ", " ^ string_of_desire desire
  ) "" l
;;

let string_of_assignment = function
  | Collect -> "Collect"
  | Return -> "Return"
  | Construct -> "Construct"
  | Blockade -> "Blockade"
  | Sabotage -> "Sabotage"
;;

let string_of_medium_destination p_ship =
  match p_ship.medium_destination with
  | None -> "no destination"
  | Some (row, col) ->
    Printf.sprintf "destination %d, %d" row col
;;

let string_of_int_int_option = function
  | None -> "None"
  | Some (row, col) ->
    Printf.sprintf "Some %d, %d" row col
;;

let string_of_target = function
  | None -> "no target"
  | Some target -> begin match target with
    | Base (entity, _) -> let row, col = entity.position in Printf.sprintf "Base at %d, %d" row col
    | Harvest (p_ship, _) -> let row, col = p_ship.entity.position in Printf.sprintf "Ship at %d, %d" row col
    | Crash_site (row, col) -> Printf.sprintf "Crash site at %d, %d" row col
  end
;;

let string_of_p_ship_assignment p_ship =
  match p_ship.assignment with
  | Collect -> "Collect " ^ (string_of_medium_destination p_ship)
  | Return -> "Return"
  | Construct -> "Construct " ^ (string_of_medium_destination p_ship)
  | Blockade -> "Blockade " ^ (string_of_target p_ship.target)
  | Sabotage -> "Sabotage" ^ (string_of_target p_ship.target)
;;

let debug_assignment p_ship =
  debug (Printf.sprintf "Ship = %s, assignment = %s\n\n" (string_of_p_ship p_ship) (string_of_p_ship_assignment p_ship));
;;


