(* OCaml Starter for Halite III on Halite.io
   This code is public domain. There is no warranty.
 *)

open Type;;

let version_string = "_v0.20.j";;

(* Add desires to state.persist.desire_map for all my ships *)
let ships_choose_desired_moves state =

  List.iter (fun p_ship ->
    p_ship.desired_moves <- [];
    p_ship.move_decided <- false;
    Mole.update_assignment state p_ship;
    (*Debug.debug_assignment p_ship;*)
  ) (Game.my_p_ships state);

  Mole.update_blockade_and_sabotage_targets state;
  Mole.ships_update_destinations state;
  Mole.print_contested state;

(*
  if state.num_players = 4 then (
*)
    Iguana.time_fn Inspire.choose_inspiration_relevant_moves state;
(*
  );
*)

  List.iter (fun p_ship ->
    if p_ship.desired_moves = [] then (
      Mole.choose_desired_moves state p_ship;
    )
  ) (Game.my_p_ships state);

  Mole.update_friendly_obstacles state;

  List.iter (fun p_ship ->
    if p_ship.desired_moves = [] then (
      Mole.choose_desired_moves state p_ship;
    )
  ) (Game.my_p_ships state);

;;

(* Receive state and return commands *)
let mybot_fun state =

  (*Debug.debug "begin";*)
  (*Debug.debug (Printf.sprintf "my_id = %d\n" state.my_id);*)
  Mole.begin_turn state;
  (*ignore (Sim.new_sim_state state);*)
  Mole.clear_obstacles state;
  Mole.update_enemy_obstacles state;
  Mole.create_area_searches state;
  state.persist.total_halite_available <- Iguana.sum_grid state.map;

  Mole.update_future_dropoff_point state;
  Mole.update_crash_search state;
  Mole.update_all_enemy_search state;
  Iguana.time_fn Iguana.update_ships_near state;
  state.persist.inspired_condensed_map <- Iguana.make_inspired_condensed_map state;
  Mole.update_stupidity state;
  List.iter (Mole.update_assignment state) (Game.my_p_ships state);
  ships_choose_desired_moves state;
  Iguana.squash_then_stretch_all_desires state;
  let ship_commands = Mole.finalise_ship_commands state in
  let other_commands = Mole.finalise_other_commands state in
    ship_commands @ other_commands

;;

(* Run bot functions with state data taken from stdin *)
let run pregame_process bot =
  try (
    (* log to file *)
    Debug.setup_logging "Mole" version_string;
    let state = Game.new_state() in
    Random.init(max (state.const.game_seed - 9823) (state.const.game_seed + 1303));
    (* I was getting poor quality random results until I started discarding
     * the first few. 
     *)
    for i = 0 to 1000 do
      ignore(Random.int(i+10));
    done;
    Networking.process_initial_tokens state;
    pregame_process state;
    Networking.send_string ("Mole" ^ version_string);
    Networking.done_sending();
    let max_seconds_elapsed = ref 0. in
    while true do
      Networking.parse_input state;
      let commands = bot state in
      let seconds_elapsed = (Game.time_seconds_elapsed_this_turn state) in
      Debug.debug (Printf.sprintf "Seconds elapsed = %f\n" seconds_elapsed);
      max_seconds_elapsed := max !max_seconds_elapsed seconds_elapsed;
      Debug.debug (Printf.sprintf "Max seconds elapsed = %f\n" !max_seconds_elapsed);
      Networking.send_commands commands;
      Debug.debug "Finished turn.\n";
    done
  )
  with e ->
    Debug.error (Printexc.to_string e);
    Debug.close_files();
;;

run Mole.pregame_process mybot_fun;;

