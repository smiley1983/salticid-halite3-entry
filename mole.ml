open Type;;

let force_set_medium_destination state p_ship =
  (*Debug.debug "force_set_medium_destination\n";*)
  let row, col = p_ship.entity.position in
  let drow, dcol = row / 4, col / 4 in
  p_ship.medium_destination <- Some (drow, dcol)
;;

let count_owned_ships_medium_destination state owner dest =
  let count = ref 0 in
  List.iter (fun p_ship ->
    if p_ship.entity.owner = owner then
      match p_ship.medium_destination with
      | None -> ()
      | Some d -> if (d = dest) && (p_ship.assignment = Collect) then 
        count := !count + 1
  ) state.persist.p_ships;
  !count
;;

let count_other_owned_ships_medium_destination state not_p_ship owner dest =
  let count = ref 0 in
  List.iter (fun p_ship ->
    if (p_ship.entity.owner = owner) 
    && (p_ship.entity.id != not_p_ship.entity.id)  then
      match p_ship.medium_destination with
      | None -> ()
      | Some d -> if (d = dest) && (p_ship.assignment = Collect) then 
        count := !count + 1
  ) state.persist.p_ships;
  !count
;;

let position_distance_to_dropoff state (row, col) =
  state.persist.dropoff_search.(row).(col)
;;

let update_contested_target state p_ship =
  let friends = Game.my_p_ships state in
  let pr, pc = p_ship.entity.position in
  let current_halite = state.map.(pr).(pc) in
  let _, best = List.fold_left (fun (acc_score, acc) (row, col) ->
    let dist = Game.calculate_distance state p_ship.entity.position (row, col) in
    let me_closer = state.persist.all_enemy_search.(row).(col) > dist in
    let others_targeting = List.fold_left (fun acc friend ->
      match friend.contested_target with
      | None -> acc
      | Some (friend_target) ->
        if friend_target = (row, col) then acc + 1 else acc
    ) 0 friends in
    let closer_factor = if me_closer then 1. else 2. in
    let score = float_of_int state.map.(row).(col) /. (closer_factor *. (1. +. float_of_int others_targeting) *. (float_of_int dist)) in
    if score > acc_score then score, Some (row, col) else acc_score, acc
  ) ((float_of_int current_halite) *. 1.5, None) p_ship.contested in
  p_ship.contested_target <- best
;;

let set_collector_medium_destination state p_ship =
  (*Debug.debug "set_collector_medium_destination \n";*)
  (*let srow, scol = p_ship.entity.position in*)
  (*Debug.flog_msg state.turn srow scol "set medium destination";*)
  (*Debug.flog_colour state.turn srow scol 255 255 0;*)
  let best = ref None in
  Array.iteri (fun ir row ->
    Array.iteri (fun ic cell ->
    (*
      match state.persist.searches.(ir).(ic) with
      | None -> ()
      | Some search ->
    *)
      let search = state.persist.searches.(ir).(ic) in
        (*Debug.debug (Printf.sprintf "Scoring search at %d, %d\n" ir ic);*)
        let other_friendly_ships = count_other_owned_ships_medium_destination state p_ship state.my_id (ir, ic) in
        (*Debug.debug (Printf.sprintf "other_friendly_ships = %d\n" other_friendly_ships);*)
        let value = (*min (state.const.max_energy * 7 / 2) *) float_of_int cell /. (max 1. (1. +. float_of_int (other_friendly_ships))) in
        (*Debug.debug (Printf.sprintf "Value = %f\n" value);*)
        let ship_row, ship_col = p_ship.entity.position in
        (*Debug.debug (Printf.sprintf "Ship at %d, %d looking at search of dimensions %d, %d\n" ship_row ship_col (Array.length search) (Array.length search.(0)));*)
        (*let dist_penalty = if Game.is_my_base_position state p_ship.entity.position then 1 else search.(ship_row).(ship_col) in*)
        let dest_row, dest_col = (ir * state.persist.condense_factor) + (state.persist.condense_factor / 2), (ic * state.persist.condense_factor) + (state.persist.condense_factor / 2) in
        let dist = search.(ship_row).(ship_col) in
        let dist_penalty = if dist = max_int then 1_000_000_000 else 
          search.(ship_row).(ship_col) + state.persist.dropoff_search.(dest_row).(dest_col) 
        in
        let score' = (* (log value) *. 150. *) value *. value /. float_of_int (max 1 (dist_penalty * dist_penalty)) in
        let score = int_of_float score' in
        (*Debug.debug (Printf.sprintf "dist = %d, dist_penalty = %d, score = %d\n" dist dist_penalty score);*)
        begin match !best with
        | None -> best := Some (score, (ir, ic))
        | Some (best_score, best_pos) ->
          if score > best_score then best := Some (score, (ir, ic))
        end
    ) row
  ) state.persist.inspired_condensed_map;
  match !best with
  | None -> force_set_medium_destination state p_ship
  | Some (score, dest) -> 
    p_ship.medium_destination <- Some dest;
    (*let drow, dcol = dest in*)
    (*Debug.debug (Printf.sprintf "Score for ship %d looking at %d, %d was %d\n" p_ship.entity.id drow dcol score);*)
    (*let srow, scol = p_ship.entity.position in*)
    (*Debug.flog_msg state.turn srow scol (Printf.sprintf "Score for ship %d looking at %d, %d was %d" p_ship.entity.id drow dcol score);*)
;;

let update_blockade_and_sabotage_targets state =
  (*Debug.debug "update_blockade_and_sabotage_targets \n";*)
  let crash_sites = List.map (fun position ->
    Crash_site position
  ) (Iguana.high_halite_tiles state) in
  let ship_targets = Iguana.ship_targets state in
  state.persist.targets <- crash_sites @ ship_targets;
  (*Debug.debug "update_blockade_and_sabotage_targets done\n";*)
;;

let raw_score_target state target =
  match target with
  | Base (base, exits) -> float_of_int (state.const.max_energy * 5)
  | Harvest (ship, threatened) -> float_of_int (ship.entity.halite)
  | Crash_site (row, col) -> float_of_int state.map.(row).(col)
;;

let target_position target =
  match target with
  | Base (base, exits) -> base.position
  | Harvest (ship, threatened) -> ship.entity.position
  | Crash_site position -> position
;;

let targets_equal t1' t2' =
  match t1' with
  | None -> t1' = t2'
  | Some t1 ->
    begin match t2' with
    | None -> false
    | Some t2 ->
      begin match t1 with 
      | Base (_, _) -> t1 = t2
      | Crash_site (_, _) -> t1 = t2
      | Harvest (p_ship, _) ->
        begin match t2 with
        | Base (_, _) | Crash_site (_, _) -> false
        | Harvest (o_ship, _) -> p_ship.entity = o_ship.entity
        end
      end
    end
;;

let other_ships_sharing_target state p_ship target =
  List.fold_left (fun acc o_ship ->
    if (o_ship.entity.owner = p_ship.entity.owner)
    && (o_ship.entity.id != p_ship.entity.id)
    && (targets_equal o_ship.target target)
    then
      acc + 1
    else acc
  ) 0 state.persist.p_ships
;;

let target_destination_count = function
  | Base (base, exits) -> List.length exits
  | Harvest (e_ship, destinations) -> List.length destinations + 1
  | Crash_site position -> 1
;;

let target_distance_to_self_base_penalty state p_ship target' =
  match target' with
  | Harvest (target, _) ->
    let search = state.persist.all_dropoff_searches.(target.entity.owner) in
    let row, col = target.entity.position in
    let d = (search.(row).(col) - Game.calculate_distance state p_ship.entity.position target.entity.position) in
    if d < 1 then 1. /. abs_float(float_of_int (d - 1))
    else 1.
  | _ -> 1.
;;

let score_blockade_sabotage_target state p_ship target =
  let raw = raw_score_target state target in
  let dist = Game.calculate_distance state p_ship.entity.position (Game.target_position target) in
  let competing_friendly = other_ships_sharing_target state p_ship (Some target) in
  let friendly_quorum = target_destination_count target in
  let enemy_return_penalty = target_distance_to_self_base_penalty state p_ship target in
  let denom = max 1 (competing_friendly - friendly_quorum) in
    ((raw /. (float_of_int dist)) /. (float_of_int denom)) *. enemy_return_penalty
;;

let set_blockade_sabotage_destination state p_ship include_base_entry =
  let best_score, best = List.fold_left (fun (acc_score, acc) target ->
    let score = score_blockade_sabotage_target state p_ship target in
    match acc with
    | None -> score, Some target
    | Some best ->
      if score > acc_score then score, Some target
      else (acc_score, acc)
  ) (0., None) state.persist.targets
  in
  p_ship.target <- best;
(*
  let destinations =
    (Iguana.high_halite_tiles state)
    @ (Iguana.high_halite_enemy_ship_positions_and_destinations state)
    @ if include_base_entry then
      Iguana.enemy_base_entry_points state
    else
      []
  in
  ignore destinations;
  ()
*)
;;

let set_medium_destination state p_ship =
  match p_ship.assignment with
  | Collect -> set_collector_medium_destination state p_ship
  | Blockade
  | Sabotage -> set_blockade_sabotage_destination state p_ship true
  | Return
  | Construct -> ()
;;
      (* This used to be at the top of the | Some search match below
      Kept as an example of Search_sorted invocation
      let sorted, final = Search_sorted.ship_search state search p_ship in
      let grid = (Iguana.normalise_int_grid (Array.map (Array.map (fun cell -> int_of_float cell.score (*cell.profit_total*))) sorted) 255) in
      Debug.debug "Decision grid:\n";
      Iguana.debug_int_grid grid;
      Debug.debug "Other grid:\n";
      Iguana.debug_int_grid search;
      *)
      (*Iguana.save_grid_picture "search_grid" grid 64;*)
(*
let grid_rank_enemy_desc_eval state grid enemy_mul (srow, scol) =
  let start_quality = grid.(srow).(scol) in
  let eval (row, col) =
    let quality = start_quality - grid.(row).(col) in
    let enemy = Game.enemy_at state (row, col) in
    if enemy then (
      int_of_float (float_of_int quality *. enemy_mul)
    )
    else (
      quality
    )
  in
    eval
;;

let grid_rank_return_desc_eval state grid enemy_mul (srow, scol) =
  let start_quality = grid.(srow).(scol) in
  let eval (row, col) =
    let halite = state.map.(row).(col) in
    let quality = start_quality - (grid.(row).(col) + (halite / 100)) in
    let enemy = Game.enemy_at state (row, col) in
    if enemy then (
      int_of_float (float_of_int quality *. enemy_mul)
    )
    else (
      quality
    )
  in
    eval
;;

let grid_rank_enemy_collect_desc_eval state grid enemy_mul (srow, scol) =
  let start_quality = grid.(srow).(scol) in
  let eval (row, col) =
    let halite = state.map.(row).(col) in
    (*
    let quality = if (Game.position_not_owned state (row, col)) then start_quality - (grid.(row).(col) - (halite / 100))(* FIXME this is not really valid, would be better to use a float and modify quality by a value between 0. and 0.99 for halite *) else -100 in
    *)
    let quality = start_quality - (grid.(row).(col) - (halite / 100)) in
    let enemy_test = if state.num_players = 4 then Game.enemy_could_occupy else Game.enemy_at in
    let enemy = enemy_test state (row, col) in
    let modified = if enemy then (
      int_of_float (float_of_int quality *. enemy_mul)
    )
    else (
      quality
    )
    in
      modified
  in
    eval
;;
*)

let row_col_distance_difference (r1, c1) (r2, c2) =
  if r1 >= c1 then (r1 - r2), (c1 - c2)
  else (c1 - c2), (r1 - r2)
;;

let rec find_target dist (row, col) =
  if dist.(row).(col) = 0 then (row, col)
  else (
    let adj = Search.get_adjacent dist (row, col) in
    let (nr, nc, lowest) = List.fold_left (fun (acc_r, acc_c, acc_dist) (r, c) ->
      let new_dist = dist.(r).(c) in
      if new_dist < acc_dist then (r, c, new_dist)
      else (acc_r, acc_c, acc_dist)
    ) (row, col, (dist.(row).(col))) adj in
    if lowest = dist.(row).(col) then (row, col)
    else find_target dist (nr, nc)
  )
;;

let calculate_dist_score state grid source consider =
  let srow, scol = source in
  let row, col = consider in
  let start_dist = grid.(srow).(scol) in
  let dist_diff = start_dist - grid.(row).(col) in
  (*let dist_score = if dist_diff < 0 then 0. else if dist_diff = 0 then 0.5 else 1. in*)
  let primary = 
    if dist_diff > 0 then 0.9 else if dist_diff = 0 then 0.5 else 0. 
  in
  let target = find_target grid consider in
  let source_dist = Game.row_col_distance state source target in 
  let consider_dist = Game.row_col_distance state consider target in
  let d_larger, d_smaller = row_col_distance_difference consider_dist source_dist in
  let secondary = 
    if d_larger > 0 then 0.
    else if d_larger = 0 then (
      if d_smaller > 0 then 0.1
      else if d_smaller = 0 then 0.5
      else 0.7
    )
    else 1.
  in
    if (grid.(row).(col) = max_int && start_dist = max_int) then
      secondary
    else
      primary +. (secondary /. 10.)
;;

let calculate_target_score state target source consider =
  let source_dist = Game.row_col_distance state source target in 
  let consider_dist = Game.row_col_distance state consider target in
  let d_larger, d_smaller = row_col_distance_difference consider_dist source_dist in
  let sum_pair (a, b) = a + b in
  let source_sum = sum_pair source_dist in
  let consider_sum = sum_pair consider_dist in
  let dist_diff = source_sum - consider_sum in
  let primary = 
    if dist_diff > 0 then 0.9 else if dist_diff = 0 then 0.5 else 0. 
  in
  let secondary = 
    if d_larger > 0 then 0.
    else if d_larger = 0 then (
      if d_smaller > 0 then 0.1
      else if d_smaller = 0 then 0.5
      else 0.7
    )
    else 1.
  in
    if (consider_sum > state.width * 2 && source_sum > state.width * 2) then
      secondary
    else
      primary +. (secondary /. 10.)
;;

let collector_dist_score state grid p_ship source consider =
  match p_ship.contested_target with
  | None -> calculate_dist_score state grid source consider
  | Some target -> calculate_target_score state target source consider
;;

let is_inspired state perspective row col = 
  let count = List.fold_left (fun acc p_ship ->
    if p_ship.entity.owner = perspective then acc else acc + 1
  ) 0 state.persist.ships_near.(row).(col) in
  count > 1
;;

let attack_okay state p_ship =
  true
;;

let risk_crash_okay state p_ship =
  true
;;

let calculate_guard_score state (row, col) =
  let dist = state.persist.all_enemy_search.(row).(col) in
  if dist > 6 then 0.01 else (
    if dist < 1 then 0.01
    else if dist = 1 then 0.02
    else let fdist = float_of_int dist in 1. /. (fdist *. fdist)
  (*
    match dist with
    | 5 -> 0.6
    | 4 -> 0.7
    | 3 -> 0.8
    | 2 -> 0.9
    | 1 -> 1.
    | _ -> 0.1
  *)
  )
;;

let calculate_enemy_score state p_ship enemy_mul (srow, scol) (row, col) =
   (*enemy_mul (* FIXME *)*)
  let enemy_mul' = Iguana.inspired_danger_score state p_ship (row, col) in
  let old_v = (
    let occupied = Game.enemy_at state (row, col) in
    if state.num_players = 2 && (not occupied) then 1.
    else if (state.num_players = 4) && (not (Game.enemy_high_risk_occupy state (row, col))) then 1.
    else enemy_mul
  ) in
     enemy_mul' *. old_v
;;

let collector_eval state grid p_ship enemy_mul (srow, scol) =
  (*let start_dist = grid.(srow).(scol) in*)
  let eval (row, col) =

    let ct' = List.nth state.persist.percentile_thresholds 3 in
    let base_dist = state.persist.dropoff_search.(row).(col) in
    let collect_threshold = max 1 (ct' - (ct' / (base_dist + 2))) in

    let inspired = is_inspired state p_ship.entity.owner row col in
    let insp_mul = if inspired then 1 else 1 in

    let mining = (row, col) = p_ship.entity.position in
    let halite = state.map.(row).(col) * insp_mul in
    (*let dist_diff = start_dist - grid.(row).(col) in*)
    (*let dist_score = if dist_diff < 0 then 0. else if dist_diff = 0 then 0.5 else 1. in*)
    let dist_score = collector_dist_score state grid p_ship (srow, scol) (row, col) in
(*      calculate_dist_score state grid (srow, scol) (row, col) 
    in *)
    let halite' = min halite state.const.max_energy in
    let halite_score = 
      let threshold_mul = if halite >= collect_threshold then 1. else 0.1 in
        (float_of_int halite') *. (if mining then 1. else if halite >= collect_threshold then 0.1 else 0.01) *. threshold_mul /. float_of_int state.const.max_energy
        (*
      else if not mining then let limit = state.const.max_energy - collect_threshold in
        (float_of_int limit /. float_of_int state.const.max_energy) -. (float_of_int halite' /. float_of_int state.const.max_energy)
      else 
        ((float_of_int halite') /. float_of_int state.const.max_energy) *. 0.1
        *)
    in
    (*Debug.debug (Printf.sprintf "ship id = %d, ct' = %d, collect_threshold = %d, halite = %d, halite' = %d, halite_score = %f mining = %b \n" p_ship.entity.id ct' collect_threshold halite halite' halite_score mining);*)
    let reward_score = float_of_int (state.const.max_energy - p_ship.entity.halite) /. float_of_int state.const.max_energy in (* wtf is this? I think it weights moves in favour of low-halite ships... if the weights are not normalised later *)
    let enemy_test = 
      Game.enemy_could_occupy
    (*
      if state.num_players = 4 
      (* 
      then
      *)
      && (state.turn > state.const.max_turns * 1 / 5) then 
        Risk.crash_likelihood_alert p_ship
        (*Game.enemy_high_risk_occupy *)
      else if (state.num_players = 4) then Game.enemy_could_occupy 
      (* *)
      else (* if (List.length (Game.my_ships state) < List.length (Game.not_my_ships state)) then Game.enemy_high_risk_occupy else*) 
        Game.enemy_at
    *)
    in
    let enemy = (*(not ((srow, scol) = (row, col))) &&*) (enemy_test state (row, col)) in
    (*let enemy = enemy_test state (row, col) in*)
    let base = not (Game.position_not_owned state (row, col)) in
    let enemy_score = 
      if enemy then
        calculate_enemy_score state p_ship enemy_mul (srow, scol) (row, col)
      else 1.
    in
    let base_score = if base then 0.00001 else 1. in
    let guard_score = calculate_guard_score state (row, col) in
    let result =
    (((dist_score *. 8.) +. (halite_score *. 7.) +. reward_score +. (guard_score *. 2.)) /. 18.) 
      *. enemy_score *. base_score
    in
    (*Debug.debug (Printf.sprintf "num_players = %d, enemy = %b, dist_score = %f, halite_score = %f, reward_score = %f, enemy_score = %f, base_score = %f\n" state.num_players enemy dist_score halite_score reward_score enemy_score base_score);*)
    result
    (*
    let modified = if enemy then (
      int_of_float (float_of_int quality *. enemy_mul)
    )
    else (
      quality
    )
    in
      modified
    *)
  in
    eval
;;

let return_eval state grid p_ship enemy_mul (srow, scol) =
  (*let start_dist = grid.(srow).(scol) in*)
  let eval (row, col) =
    let inspired = is_inspired state state.my_id row col in
    let halite = if inspired then state.map.(row).(col) (* * 3 *) else state.map.(row).(col) in
    (*let dist_diff = start_dist - grid.(row).(col) in*)
    let dist_score = (*if dist_diff < 0 then 0. else if dist_diff = 0 then 0.5 else 1.*) calculate_dist_score state grid (srow, scol) (row, col) in
    let halite_score = float_of_int (max 0 (state.const.max_energy - halite)) /. float_of_int state.const.max_energy in
    let reward_score = float_of_int (p_ship.entity.halite) /. float_of_int state.const.max_energy in
    let enemy_test = (*if state.num_players = 4 then*) Game.enemy_could_occupy (*else Game.enemy_at *) in
    let enemy = (not ((srow, scol) = (row, col))) 
      && (not (Game.is_my_base_position state (row, col)))
      && (enemy_test state (row, col)) 
    in
    let base = not (Game.position_not_owned state (row, col)) in
    let enemy_mul'' = if (row, col) = (srow, scol) then enemy_mul /. 10. else enemy_mul in
    let enemy_mul' = Iguana.inspired_danger_score state p_ship (row, col) *. enemy_mul'' in
    let enemy_score = if enemy then enemy_mul' else 1. in
    let base_score = if base then 1. else 0.9 in
    let still_penalty = if (row, col) = (srow, scol) && (* (p_ship.prev_position = p_ship.entity.position) *) (p_ship.turns_stationary > 2) then 0.1 else 1. in
    let result = ((dist_score +. halite_score +. reward_score) /. 3.) 
      *. enemy_score *. base_score *. still_penalty
    in
    (*Debug.debug (Printf.sprintf "dist_score = %f, halite_score = %f, reward_score = %f, enemy_score = %f, base_score = %f, still_penalty = %f\n" dist_score halite_score reward_score enemy_score base_score still_penalty);*)
    result
  in
    eval
;;

let target_eval state p_ship enemy_mul source target =
  let row_dist, col_dist = Game.row_col_distance state source target in
  let max_dist = max row_dist col_dist in
  let total_dist = row_dist + col_dist in
  let eval (row, col) =
    let next_rowdist, next_coldist = Game.row_col_distance state (row, col) target in
    let next_max_dist = max next_rowdist next_coldist in
    let next_total_dist = next_rowdist + next_coldist in
    let dist_score = total_dist - next_total_dist in
    let bonus_score = max_dist - next_max_dist in
    let primary = float_of_int (dist_score + 1) (*/. 2.*) in
    let bonus = float_of_int (bonus_score + 1) (*/. 2.*) in
    let enemy_score = if state.persist.under_threat.(row).(col) then enemy_mul else 1. in

    ((primary *. 10. +. bonus) /. 11.) *. enemy_score
  in
    eval
;;

let crash_value state =
  (List.nth state.persist.percentile_thresholds 4) * 2
;;

let desire_seek_high_halite_ships_and_positions state p_ship =
  let my, en = Iguana.nearship_balance state p_ship.entity.position in
  let ratio = (float_of_int my) /. (float_of_int en) in
  let bravery = if ratio >= 4.0 then 1. else ratio /. 5. in
  let eval = collector_eval state state.persist.crash_search p_ship bravery p_ship.entity.position in
  Iguana.eval_add_desires state p_ship eval (Iguana.multiply_weights_int 1.)
;;

let rec add_crash_desires state p_ship =
  desire_seek_high_halite_ships_and_positions state p_ship;
  true
(* 
  if (state.num_players != 2) then false
  else (
*)
(*
    Debug.debug "add_crash_desires\n";
    let row, col = p_ship.entity.position in
    match List.fold_left (fun acc nearship ->
      begin match acc with
      | None ->
        if nearship.entity.owner = p_ship.entity.owner then None
        else Some nearship
      | Some best ->
        if nearship.entity.owner = p_ship.entity.owner then acc
        else if best.entity.halite > nearship.entity.halite then acc
        else Some nearship
      end
    ) None state.persist.ships_near.(row).(col)
    with
    | None -> 
      desire_seek_high_halite_ships_and_positions state p_ship;
      true
    | Some ship_with_highest_halite ->
      if ship_with_highest_halite.entity.halite < p_ship.entity.halite * 2 then false
      else (
        let eval = target_eval state p_ship 1. p_ship.entity.position ship_with_highest_halite.entity.position in
        Iguana.eval_add_desires state p_ship eval (Iguana.multiply_weights_int 1.);
        true
      )
*)
(*  ) *)
;;

let global_end_is_nigh state =
  Game.turns_remaining state < (state.width * 4 / 3)
;;

let add_collection_search_desires state p_ship search (row, col) =
  let multiplier = 2. in
  let srow, scol = p_ship.entity.position in
  (*Debug.flog_msg state.turn srow scol (Printf.sprintf "dest area = %d, %d" (row * state.persist.condense_factor + state.persist.condense_factor / 2) (col * state.persist.condense_factor + state.persist.condense_factor / 2));*)
  let base_dist = max 1 state.persist.dropoff_search.(srow).(scol) in
  let enemy_mul = (1. /. (float_of_int (base_dist * base_dist))) (* *. 0.1 *) *. Iguana.bravery state p_ship (* *. state.persist.stupidity *) in
  let eval = collector_eval state search p_ship enemy_mul (srow, scol) in
  Iguana.eval_add_desires state p_ship eval (Iguana.multiply_weights_int multiplier)
;;

let returner_get_eval state p_ship =
  if p_ship.assignment = Return then
    let row, col = p_ship.entity.position in
    let base_dist = max 1 state.persist.dropoff_search.(row).(col) in
    let enemy_mul = (1. /. (float_of_int (base_dist * base_dist))) *. Iguana.bravery state p_ship (* *. state.persist.stupidity *) in
    let eval = return_eval state state.persist.dropoff_search p_ship enemy_mul (row, col) in
      Some eval
  else None
;;

let collector_get_eval state p_ship =
  if p_ship.assignment = Collect then
    match p_ship.medium_destination with 
    | None -> None
    | Some (row, col) ->
    (*
      begin match state.persist.searches.(row).(col) with
      | None -> None
      | Some search ->
    *)
      let search = state.persist.searches.(row).(col) in
        let srow, scol = p_ship.entity.position in
        let base_dist = max 1 state.persist.dropoff_search.(srow).(scol) in
        let enemy_mul = (1. /. (float_of_int (base_dist * base_dist))) *. Iguana.bravery state p_ship (* *. state.persist.stupidity *) in
          Some (collector_eval state search p_ship enemy_mul (srow, scol))
      (*end*)
  else None
;;

let rec add_collection_desires state p_ship =
  (*Debug.debug (Printf.sprintf "Crash value = %d\n" (crash_value state));*)
  if ((crash_value state < (400 / (state.num_players * state.num_players / 2 ) ) ) || (global_end_is_nigh state && p_ship.entity.halite < state.persist.magic.endgame_high_halite / 4))
  && (add_crash_desires state p_ship) then () 
  else (
    (*Debug.debug "add_collection_desires\n";*)
    match p_ship.medium_destination with 
    | None ->
      (*Debug.debug "no destination\n";*)
      set_medium_destination state p_ship;
      add_collection_desires state p_ship;
    | Some (row, col) ->
      let search = state.persist.searches.(row).(col) in
      (*begin match state.persist.searches.(row).(col) with*)
      (*| None -> *)
        (*Debug.debug "no search in destination area\n";*)
        (*set_medium_destination state p_ship;*)
        (*add_collection_desires state p_ship;*)
      (*| Some search ->*)
        add_collection_search_desires state p_ship search (row, col)
      (*end*)
  )
;;

let no_friendly_ship_at state position =
  not (List.exists (fun ship ->
    ship.owner = state.my_id
    && (ship.position = position)
  ) state.ships)
;;

let nearest_not_friendly_occupied state p_ship positions =
  let unoccupied = List.filter (no_friendly_ship_at state) positions in
  let final_list = 
    if List.length unoccupied > 0 then unoccupied
    else positions
  in
  List.fold_left (fun acc position ->
    match acc with
    | None -> Some position
    | Some best ->
      let best_dist = Game.calculate_distance state p_ship.entity.position best in
      let test_dist = Game.calculate_distance state p_ship.entity.position position in
      if (test_dist < best_dist) then Some position
      else acc
  ) None final_list
;;

let get_eval_move_to_nearest_not_friendly_occupied state p_ship enemy_mul positions =
  match nearest_not_friendly_occupied state p_ship positions with
  | None ->
    (*Debug.debug "Warning! move_to_nearest_not_friendly_occupied falling back on add_collection_desires!\n";*)
    None;
  | Some best ->
    let eval = target_eval state p_ship enemy_mul p_ship.entity.position best in
    Some eval
;;

let move_to_nearest_not_friendly_occupied state p_ship enemy_mul positions =
  match get_eval_move_to_nearest_not_friendly_occupied state p_ship enemy_mul positions with
  | None ->
    p_ship.assignment <- Collect;
    add_collection_desires state p_ship
  | Some eval ->
    Iguana.eval_add_desires state p_ship eval (Iguana.multiply_weights_int 1.)
;;

let add_sabotage_desires state p_ship =
  match p_ship.target with
  | None ->
    (* target should have been assigned before this point *)
    (*Debug.debug(Printf.sprintf "Changing from Sabotage to Collect due to lack of target: %s\n" (Debug.string_of_p_ship p_ship));*)
    p_ship.assignment <- Collect;
    add_collection_desires state p_ship
  | Some target ->
    (*Debug.debug "sabotage target found\n";*)
    begin match target with
    | Base (base, exits) -> move_to_nearest_not_friendly_occupied state p_ship 1. exits
    | Harvest (e_ship, destinations) -> move_to_nearest_not_friendly_occupied state p_ship 1. destinations
    | Crash_site position -> move_to_nearest_not_friendly_occupied state p_ship 1. [position]
    end
;;

let get_sabotage_eval state p_ship =
  match p_ship.target with
  | None -> None
    (* target should have been assigned before this point *)
    (*Debug.debug(Printf.sprintf "Changing from Sabotage to Collect due to lack of target: %s\n" (Debug.string_of_p_ship p_ship));*)
    (*p_ship.assignment <- Collect;*)
    (*add_collection_desires state p_ship*)
  | Some target ->
    (*Debug.debug "sabotage_eval target found\n";*)
    begin match target with
    | Base (base, exits) ->
      get_eval_move_to_nearest_not_friendly_occupied state p_ship 1. exits
    | Harvest (e_ship, destinations) ->
      get_eval_move_to_nearest_not_friendly_occupied state p_ship 1. destinations
    | Crash_site position ->
      get_eval_move_to_nearest_not_friendly_occupied state p_ship 1. [position]
    end
;;

let construction_destination_distance state p_ship position =
  match p_ship.medium_destination with
  | None -> max_int
  | Some dest ->
    Game.calculate_distance state position dest
;;

let construction_destination_distance_pship state p_ship =
  match p_ship.medium_destination with
  | None -> max_int
  | Some dest ->
    Game.calculate_distance state p_ship.entity.position dest
;;

let construction_destination state p_ship =
  match p_ship.medium_destination with
  | None -> p_ship.entity.position
  | Some dest -> dest
;;

let construction_destination_to_dropoff_distance state p_ship =
  match p_ship.medium_destination with
  | None -> max_int
  | Some (row, col) ->
    state.persist.dropoff_search.(row).(col)
;;

let halite_under_pship state p_ship =
  let row, col = p_ship.entity.position in
  state.map.(row).(col)
;;

let add_construction_desires state p_ship =
  let begin_distance = construction_destination_distance_pship state p_ship in
  if (begin_distance < 1) && (state.energy.(state.my_id) + p_ship.entity.halite + halite_under_pship state p_ship > state.const.dropoff_cost) && (state.persist.constructed_this_turn = 0) && (Game.position_not_owned state p_ship.entity.position) then (
    Iguana.add_construction_desires state p_ship;
    state.persist.next_dropoff_positions <- 
      List.filter (fun pos -> not (pos = p_ship.entity.position)) state.persist.next_dropoff_positions;
    state.persist.constructed_this_turn <- state.persist.constructed_this_turn + 1;
    state.persist.construction_decided <- false;
    List.iter (fun p_ship ->
      if p_ship.assignment = Construct then (
        p_ship.assignment <- Collect;
        p_ship.medium_destination <- None;
      )
    ) (Game.my_p_ships state)
  )
  else (
  (*
    let weighted = Iguana.weight_directions state p_ship (fun position ->
      (float_of_int (begin_distance - construction_destination_distance state p_ship position))
    ) in
  *)
    let enemy_mul = 0.00001 in
    let eval = target_eval state p_ship enemy_mul p_ship.entity.position (construction_destination state p_ship) in
    Iguana.eval_add_desires state p_ship eval (Iguana.multiply_weights_int 1.)
    (*Iguana.add_weighted_desires state p_ship weighted 1.*)
  )
;;


let dropoff_search_for_my_p_ship state p_ship =
  let row, col = p_ship.entity.position in
  let base_dist = state.persist.dropoff_search.(row).(col) in
  if base_dist < 3 then state.persist.dropoff_search
  else if state.persist.safe_dropoff_search.(row).(col) < state.width * 2 then 
    state.persist.safe_dropoff_search
  else state.persist.dropoff_search
;;

let choose_desired_moves state p_ship =
  (*Debug.debug "choose_desired_moves\n";*)
  (*let sorted, final = Search_sorted.undirected_ship_search state p_ship in*)
  (*let grid = (Iguana.normalise_int_grid (Array.map (Array.map (fun cell -> int_of_float cell.score (*cell.profit_total*))) sorted) 255) in*)
  (*Iguana.save_grid_picture "search_grid" grid 64;*)
  (*ignore (sorted, final);*)
  let row, col = p_ship.entity.position in
  let inspired = is_inspired state state.my_id row col in
  let insp_mul = if inspired then 1 else 1 in
  let cannot = (Iguana.cannot_move state p_ship.entity) in
  let ct' = List.nth state.persist.percentile_thresholds 3 in
  let base_dist = state.persist.dropoff_search.(row).(col) in
  let collect_threshold = max 1 (ct' - (ct' / (base_dist + 2))) in
  (*Debug.debug (Printf.sprintf "ct' = %d, base_dist = %d, collect_threshold = %d\n" ct' base_dist collect_threshold);*)
  (*Debug.debug (Printf.sprintf "ship %s cannot = %b\n" (Debug.string_of_p_ship p_ship) cannot);*)
  if cannot then (
    let desire = Iguana.desire_stay_still p_ship 1_000_000. in
    (*Debug.debug (Printf.sprintf "Ship cannot move: %s\n" (Debug.string_of_p_ship p_ship));*)
    Iguana.add_desire state p_ship desire
  )
  else if ((p_ship.assignment = Collect)
      && (state.map.(row).(col) * insp_mul) >= collect_threshold
      (*
        ((state.map.(row).(col) > state.const.max_energy / 10)
        || (p_ship.entity.halite >= state.const.max_energy * 2 / 3))
      *)
     && (not p_ship.inspiration_relevant)
     )
  || (((state.map.(row).(col) * insp_mul) >= List.nth state.persist.percentile_thresholds 2)
      && (p_ship.entity.halite <= state.const.max_energy - (state.map.(row).(col) * (*insp_mul*) 1) / 4) (* try dividing by 8 or 16 instead of 4 for greedier collection*)
      && (p_ship.assignment != Construct)
      && (not p_ship.in_danger)
      (*&& (not p_ship.inspiration_relevant)*)
     )
  then (
    let eval = collector_get_eval state p_ship in
    match eval with
    | None -> Iguana.add_evenly_weighted_desires state p_ship [Still] 1.
    | Some eval' ->
      Iguana.add_primary_plus_weighted_desires state p_ship eval' Still 1.2 1.
  )
  else (
    if p_ship.assignment = Collect then (
      add_collection_desires state p_ship
    )
    else if p_ship.assignment = Construct then (
      add_construction_desires state p_ship
    )
    else if p_ship.assignment = Sabotage then (
      add_sabotage_desires state p_ship
    )
    else if p_ship.assignment = Return then (
      if state.persist.obstacles_updated then (
        let base_dist = max 1 state.persist.dropoff_search.(row).(col) in
        let enemy_mul = if global_end_is_nigh state && (base_dist < 3) then 1. else (1. /. (float_of_int (base_dist * base_dist))) *. Iguana.bravery state p_ship (* *. state.persist.stupidity *) in
        let which_dropoff_search = dropoff_search_for_my_p_ship state p_ship in
        let eval = return_eval state which_dropoff_search p_ship enemy_mul (row, col) in
        Iguana.eval_add_desires state p_ship eval (Iguana.multiply_weights_int 4.)
      )
    )
  )
;;

let return_threshold state p_ship =
  let good_tile_value = List.nth state.persist.percentile_thresholds 2 in
  let dangerous_load = Magic.mul_int good_tile_value state.persist.magic.dangerous_load_mul in
  let row, col = p_ship.entity.position in
  let return_steps = state.persist.dropoff_search.(row).(col) in
  (*let return_index = (max 1 (return_steps / 2)) + 1 in*)
  let max_practical = state.const.max_energy * 91 / 100 in
  let reduction = if return_steps < 3 then max_practical / 2 else 0 in
  min dangerous_load (max_practical - reduction)
  (*state.const.max_energy * 2 / 3 *)
;;

let end_is_nigh state p_ship =
  let row, col = p_ship.entity.position in
  let result = (state.persist.dropoff_search.(row).(col) + (state.width / 2)) > (Game.turns_remaining state) in
  (*if result then*)
    (*Debug.debug (Printf.sprintf "dropoff distance = %d\n" (state.persist.dropoff_search.(row).(col) * 6 / 5));*)
  result
;;

let update_assignment state p_ship =
  (*Debug.debug("update_assignment\n");*)
  if p_ship.move_decided then ()
  else if (p_ship.entity.halite < 1) && (p_ship.assignment = Return) then (
    match state.persist.next_dropoff_positions with
    | [] ->
      if (global_end_is_nigh state && p_ship.entity.halite < state.persist.magic.endgame_high_halite / 4) then
        p_ship.assignment <- Sabotage
      else
        p_ship.assignment <- Collect
    | position :: tail -> (*FIXME*)
      p_ship.assignment <- Construct;
      p_ship.medium_destination <- Some position
  )
  else if p_ship.assignment = Sabotage && (p_ship.entity.halite > state.const.max_energy / 3) then (
    p_ship.assignment <- Return;
    p_ship.medium_destination <- None;
  )
  else if p_ship.assignment = Sabotage && (p_ship.target = None) then (
    p_ship.assignment <- Collect;
    p_ship.medium_destination <- None;
  )
  else if p_ship.assignment = Construct
  && (construction_destination_distance_pship state p_ship > construction_destination_to_dropoff_distance state p_ship) then (
    p_ship.assignment <- Collect;
    p_ship.medium_destination <- None;
  )
  else if (p_ship.assignment = Collect)
  && ((p_ship.entity.halite > return_threshold state p_ship)
    || (p_ship.entity.halite > state.const.max_energy / 20 
      && end_is_nigh state p_ship (*&& (halite_under_pship state p_ship < p_ship.entity.halite * 2) *) ))
  then (
    p_ship.assignment <- Return;
    p_ship.medium_destination <- None;
    (*let srow, scol = p_ship.entity.position in*)
    (*Debug.flog_msg state.turn srow scol "Removed medium_destination";*)
    (*Debug.flog_colour state.turn srow scol 0 255 255;*)
  );
;;

let rec ships_update_destinations state =
  let changed = ref false in
  List.iter (fun p_ship ->
    let prev_destination = p_ship.medium_destination in
    let prev_contested = p_ship.contested_target in
    update_contested_target state p_ship;
    set_medium_destination state p_ship;
    if not (
      (prev_destination = p_ship.medium_destination)
      && (prev_contested = p_ship.contested_target)
    ) then (
      (*Debug.debug (Printf.sprintf "prev medium dest = %s, prev contested target = %s\n" (Debug.string_of_int_int_option prev_destination) (Debug.string_of_int_int_option prev_contested));*)
      (*Debug.debug (Printf.sprintf "now medium dest = %s, now contested target = %s\n" (Debug.string_of_int_int_option p_ship.medium_destination) (Debug.string_of_int_int_option p_ship.contested_target));*)
      changed := true;
    )
  ) (Game.my_p_ships state);
  if !changed && (Game.time_seconds_elapsed_this_turn state < 0.65) then (
    ships_update_destinations state
  )
;;

let count_grid_tiles condition grid =
  Array.fold_left (fun acc1 row -> Array.fold_left (fun acc2 cell ->
    if condition cell then acc2 + 1
    else acc2
  ) acc1 row) 0 grid
;;

let count_balance state balance_grid =
  (count_grid_tiles (fun v -> v = state.my_id) balance_grid) - 
  (count_grid_tiles (fun v -> v != state.my_id) balance_grid) 
;;

(*

let hypothetical_voronoi_balance state additional_start_point =
  let dropoff_positions_owned = List.map (fun base ->
    base.position, base.owner
  ) (state.shipyards @ state.dropoffs) in
  let balance_dist, balance = Iguana.voronoi_search state (additional_start_point :: dropoff_positions_owned) in
  count_balance state balance
;;

let find_boundary grid value inner =
  let _, boundary = Array.fold_left (fun (ir, acc) row ->
    ((ir + 1), Array.fold_left (fun (ic, acc2) cell -> 
      ic + 1, (if inner then (
        if cell = value
        && List.exists (fun adj_v -> adj_v != value) 
            (Search.get_adjacent grid (ir, ic))
        then
          (ir, ic) :: acc2
        else acc2
      )
      else (
        if cell != value
        && List.exists (fun adj_v -> adj_v = value) 
            (Search.get_adjacent grid (ir, ic))
        then
          (ir, ic) :: acc2
        else acc2
        :: acc2
      ))
    ) (0, acc) row)
  ) (0, []) grid in
  boundary
;;

let find_most_promising_boundary state owned =
  let boundary = find_boundary owned state.my_id false in
  let best_score, best = List.fold_left (fun (best_score, best) (nrow, ncol) ->
    let score = Iguana.score_boundary_opponent_side state (nrow, ncol) in
    match best with
    | None -> score, Some (nrow, ncol)
    | Some _ -> 
      if score > best_score then score, Some (nrow, ncol)
      else best_score, best
  ) (0, None) boundary in
  best
(*
  let start_balance =  count_balance state owned in
  let promising = ref None in
  let delta = ref min_int in
  Array.iteri (fun ir row -> Array.iteri (fun ic cell ->
    if cell = state.my_id then (
      let adj = Search.get_adjacent owned (ir, ic) in
      if List.exists (fun v -> v != cell) adj then (
        let next_balance = hypothetical_voronoi_balance state ((ir, ic), state.my_id) in
        let next_delta - next_balance - start_balance in
        if next_delta > !delta then (
          delta := next_delta;
          promising := Some (ir, ic)
        )
      )
    )
  ) row) owned;
  !promising
*)
;;

*)

(*

let dropoff_good_location_search state =
  let dropoff_positions_owned = List.map (fun base ->
    base.position, base.owner
  ) (state.shipyards (*@ state.dropoffs*) ) in
  let balance_dist, balance = Iguana.voronoi_search state dropoff_positions_owned in
  let promising = find_most_promising_boundary balance condensed_map in
  let debug_string =
    match promising with
    | None -> "None"
    | Some ((row, col), owner) -> Printf.sprintf "%d, %d owned by %d" row col owner
  in
  (Printf.sprintf);
;;

*)

let best_condensed_map_points state =
  let best = ref [] in
  let best_value = ref min_int in
  Array.iteri (fun ir row -> Array.iteri (fun ic cell ->
    if cell > !best_value then (
      best_value := cell;
      best := [(ir, ic)];
    )
    else if cell = !best_value then (
      best := (ir, ic) :: !best
    )
  ) row) state.persist.condensed_map;
  !best
;;

let uncondense_centre state (row, col) =
  let factor = state.persist.condense_factor in
  let half = factor / 2 in
  row * factor + half, col * factor + half
;;

let first_or_second_closest state anchor points =
  let points' = List.sort (fun p1 p2 ->
    Game.calculate_distance state p1 anchor - Game.calculate_distance state p2 anchor
  ) points in
  match points' with
  | p1 :: p2 :: tail -> p2
  | p1 :: tail -> p1
  | [] -> raise Not_found
;;

let not_too_close_to_existing_dropoff state (row, col) =
  not (Array.exists (fun search ->
    search.(row).(col) < state.persist.magic.enemy_dropoff_exclusion_radius
  ) state.persist.all_dropoff_searches)
;;

let not_too_close_to_existing_or_future_dropoff state (row, col) =
  not (Array.exists (fun search ->
    search.(row).(col) < state.persist.magic.enemy_dropoff_exclusion_radius
  ) state.persist.all_dropoff_searches)
  && (
    state.persist.dropoff_search.(row).(col) >= state.persist.magic.friendly_dropoff_exclusion_radius
  )
  && (
    not (List.exists (fun pos ->
      Game.calculate_distance state (row, col) pos < state.persist.magic.friendly_dropoff_exclusion_radius
    ) state.persist.next_dropoff_positions)
  )
;;

let calculate_collection_radius state =
(*  int_of_float(sqrt (float_of_int state.width)) * 2 - 2 *)
  int_of_float(sqrt (float_of_int state.width)) + 2
;;

let rec tiles_within_radius = function
  | 0 -> 0
  | 1 -> 1
  | n -> 2 + ((((n - 1) * 2) - 1) * 2) + tiles_within_radius (n - 1)
;;

let calculate_dropoff_distance_penalty state collection_radius distance =
  let tiles = tiles_within_radius collection_radius in
  let average_per_tile = float_of_int state.persist.total_halite_available /. (float_of_int (state.width * state.height)) in
  let penalty_per_step = (average_per_tile *. float_of_int tiles) /. 10. in
  let penalty = 
    if distance < 0 then
      ((abs_float (float_of_int distance)) *. 1.6 *. penalty_per_step)
    else
      ((abs_float (float_of_int distance)) *. 0.5 *. penalty_per_step)
  in
    int_of_float penalty
;;

let find_dropoff_point state =
  let collection_radius = calculate_collection_radius state in
  (*let best_condensed = uncondense_centre state (first_or_second_closest state (Game.my_shipyard_position state) (best_condensed_map_points state)) in*)
  let best = ref None in
  Array.iteri (fun ir row -> Array.iteri (fun ic cell ->
    if state.persist.dropoff_search.(ir).(ic) > collection_radius * 3 / 2 
    (*&& (Game.rogue_distance state best_condensed (ir, ic) < collection_radius)*)
    && (not_too_close_to_existing_or_future_dropoff state (ir, ic))
    (*&& (not_too_close_to_existing_dropoff state (ir, ic))*)
    (*&& (Game.position_not_owned state (ir, ic))*)
    then (
      (*Debug.debug (Printf.sprintf "dropoff distance at %d, %d = %d\n" ir ic state.persist.dropoff_search.(ir).(ic));*)
      let sum_total = ref 0 in
      let dist = Array.map (Array.map (fun _ -> max_int)) state.map in
      let collection_threshold = List.nth state.persist.percentile_thresholds 3 in
      let visit = Search.sum_dist_visit state.map dist collection_radius collection_threshold sum_total in
      let get_next = Search.grid_get_next dist in
      Search.bfs (Iguana.dist_init dist [(ir, ic)]) visit get_next;
      let dist_penalty' = calculate_dropoff_distance_penalty state collection_radius (state.persist.dropoff_search.(ir).(ic) - collection_radius * 3 / 2) in
      let dist_penalty = (dist_penalty' (* * dist_penalty' *) (* * 100 *) ) in
      let score = max 1. ((float_of_int !sum_total) -. float_of_int dist_penalty) in
      match !best with
      | None ->
        best := Some (!sum_total, score, (ir, ic))
      | Some (prev_best_sum, prev_best_score, _) ->
        if score > prev_best_score then
          best := Some (!sum_total, score, (ir, ic))
    )
  ) row) state.map;
  !best
;;

(*
let find_first_dropoff_point state =
  state.persist.next_dropoff_position <- None; (* FIXME revisit *)
;;
*)

let pregame_process state =
  state.persist.magic <- Magic.new_magic (Some state);
  Iguana.init_searches state;
  state.persist.condensed_area_under_threat <- 
    Array.map (Array.map (fun _ -> false)) state.map; (* FIXME this makes a larger map than it should *)
  state.persist.under_threat <- 
    Array.map (Array.map (fun _ -> false)) state.map;
  state.persist.crash_risk_occupied <- Array.map (fun _ -> Array.init (state.persist.risk_turns + 1) (fun _ -> (0, 0)) ) state.energy;
  state.persist.crash_risk_unoccupied <- Array.map (fun _ -> Array.init (state.persist.risk_turns + 1) (fun _ -> (0, 0))) state.energy;
  state.persist.crash_risk_attack_halite <- Array.map (fun _ -> []) state.energy;
;;

let found_dropoff_string = function
  | None -> "None\n"
  | Some (total, score, (row, col)) ->
    Printf.sprintf "Some Total = %d, Score = %f, position = %d, %d\n" total score row col
;;

let find_best_existing_dropoff_score state =
  (*let collection_radius = state.width / 4 in*)
  let collection_radius = 1 + calculate_collection_radius state in
  let positions = (Game.my_shipyard_position state) :: (Game.my_dropoff_positions state) in
  List.fold_left (fun acc position ->
    let sum_total = ref 0 in
    let dist = Array.map (Array.map (fun _ -> max_int)) state.map in
    let collection_threshold = List.nth state.persist.percentile_thresholds 3 in
    let visit = Search.sum_dist_visit state.map dist collection_radius collection_threshold sum_total in
    let get_next = Search.grid_get_next dist in
    Search.bfs (Iguana.dist_init dist [position]) visit get_next;
    match acc with
    | None ->
      Some (!sum_total, position)
    | Some (best, best_pos) ->
      if !sum_total > best then
        Some (!sum_total, position)
      else acc
  ) None positions
;;

let check_dropoff_advisable state position_option =
  match position_option with
  | None -> false, false
  | Some (halite_score, score, (row, col)) ->
    let best_existing = find_best_existing_dropoff_score state in
    begin match best_existing with
    | None -> 
      (*Debug.error "No best_existing found in check_dropoff_advisable!\n";*)
      false, false (* This should never happen! *)
    | Some (existing_score, existing_pos) ->
      (*Debug.debug (Printf.sprintf "Exsiting dropoff score = %d, proposed halite = %d, proposed score = %f, ratio=%f\n" existing_score halite_score score (float_of_int halite_score /. (float_of_int existing_score)));*)
      let mining_turns = Iguana.mining_turns_to_build_new_ship state in
      let payoff_time = (state.width / 8 + mining_turns) * 5 / (max 1 ((List.length (Game.my_ships state)) / 4)) in (* FIXME magic numbers *)
      (*Debug.debug (Printf.sprintf "Ship mining time = %d, payoff time = %d\n" mining_turns payoff_time);*)
      let num_mine = float_of_int (List.length (Game.my_ships state)) in
      let num_not_mine = float_of_int (Game.largest_single_ship_count state) in
      let ship_ratio = (*min (1. /. float_of_int state.num_players)*) num_mine /. (num_mine +. num_not_mine) in
      let my_portion = int_of_float (float_of_int halite_score *. ship_ratio) in
      let required_score = Magic.mul_int existing_score (if state.persist.construction_decided then state.persist.magic.advisable_dropoff_continuation_ratio else state.persist.magic.advisable_dropoff_quality_ratio) in
      let almost_required_score = Magic.mul_int existing_score state.persist.magic.advisable_dropoff_continuation_ratio in
      (*let required_score = state.const.dropoff_*)

      let almost_advisable = (my_portion > (state.const.dropoff_cost + state.const.new_entity_energy_cost) * 3 / 2)
      (*let almost_advisable = (score > state.const.dropoff_cost * 7 / 4)*)
        && (List.length (Game.my_ships state) >= state.persist.magic.min_ships_before_dropoff (*if state.width <= 40 then state.width / 4 else 11*) )
        && (payoff_time < Game.turns_remaining state)
        && halite_score > almost_required_score
      in
      almost_advisable, (almost_advisable && (halite_score > required_score  (* was * 4 *)))

    end
;;

let cancel_construction_assignments state =
  List.iter (fun p_ship ->
    if (p_ship.assignment = Construct) then (
      p_ship.assignment <- Collect;
      p_ship.medium_destination <- None;
    )
  ) (Game.my_p_ships state)
;;

let update_construction_assignments state =
  match state.persist.next_dropoff_positions with
  | [] -> ()
  | position :: tail ->
    List.iter (fun p_ship ->
      if (p_ship.assignment = Construct) then (
        p_ship.medium_destination <- Some position; (*FIXME*)
      )
    ) (Game.my_p_ships state);
    let sorted = List.stable_sort (fun a b ->
      let a_dist = Game.calculate_distance state a.entity.position position in
      let b_dist = Game.calculate_distance state b.entity.position position in
      a_dist - b_dist
    ) (Game.my_p_ships state) in
    let dr, dc = position in
    let dhalite = state.map.(dr).(dc) in
    let my_halite = Game.my_halite state in
    List.iteri (fun i p_ship ->
      let row, col = p_ship.entity.position in
      if i < (min 8 ((List.length (Game.my_ships state) * 3 / 2)))
      && (Game.calculate_distance state p_ship.entity.position position < state.persist.dropoff_search.(row).(col))
      && (dhalite + my_halite + p_ship.entity.halite > state.const.dropoff_cost)
      then (
        p_ship.assignment <- Construct;
        p_ship.medium_destination <- Some position;
      )
    ) sorted
;;

let count_active_dropoffs state =
  let collect_threshold = max 1 (List.nth state.persist.percentile_thresholds 3) in
  let dropoff_radius = 8 in
  List.fold_left (fun acc dropoff_position ->
    let sum, count = Iguana.sum_and_count_locations_atleast_value_within_bfs_dist state state.map collect_threshold dropoff_position dropoff_radius in
    if sum > state.const.dropoff_cost then acc + 1 else acc
  ) 0 (Game.my_dropoff_positions state)
;;

let find_max_sensible_new_dropoff_points state =
  let num_active_dropoffs = count_active_dropoffs state in
  let min_ships_per_dropoff = 9 in
  let max_ships_per_dropoff = 24 in
  let num_my_ships = List.length (Game.my_ships state) in
  (*let ships_per_dropoff = float_of_int (num_my_ships) /. (float_of_int (List.length (Game.my_dropoff_positions state) + 1)) in*)
  let min_desired_dropoffs = num_my_ships / max_ships_per_dropoff in
  let max_desired_dropoffs = num_my_ships / min_ships_per_dropoff in
  if min_desired_dropoffs > num_active_dropoffs then 
    min_desired_dropoffs - num_active_dropoffs
  else if max_desired_dropoffs > 0 && (num_active_dropoffs = 0) then 1
  else 0
;;

let add_future_dropoff_point state =
  let best_dropoff_point = Iguana.time_fn find_dropoff_point state in
  (*Debug.debug (found_dropoff_string best_dropoff_point);*)
  let almost_advisable, dropoff_advisable = check_dropoff_advisable state best_dropoff_point in
  (*Debug.debug (Printf.sprintf "dropoff advisable = %b\n" dropoff_advisable);*)
  if dropoff_advisable then begin match best_dropoff_point with
  | None -> ()
  | Some (_, _, dropoff) ->
    state.persist.next_dropoff_positions <- dropoff :: state.persist.next_dropoff_positions;
    state.persist.construction_decided <- true;
    update_construction_assignments state;
  end
  else (
    if almost_advisable then state.persist.save_for_dropoff <- true;
    state.persist.next_dropoff_positions <- [];
    state.persist.construction_decided <- false;
    cancel_construction_assignments state
  )
;;

let rec add_n_future_dropoff_points state n =
  if n > 0 then (
    add_future_dropoff_point state;
    (*add_n_future_dropoff_points state (n - 1)*)
  )
;;

let update_future_dropoff_point state =
(*
  match state.persist.next_dropoff_position with
  | None ->
*)
  state.persist.next_dropoff_positions <- [];
  let max_num_dropoff_points = find_max_sensible_new_dropoff_points state in
  add_n_future_dropoff_points state max_num_dropoff_points;
    (*
    *)
(*
  | Some dropoff -> ()
*)
;;

let enough_halite_to_afford_ship state =
  let extra_cost = max 
    (List.length state.persist.next_dropoff_positions * state.const.dropoff_cost) 
    (if (state.persist.save_for_dropoff) then state.const.dropoff_cost else 0) 
  in
  (*
  let extra_cost = match state.persist.next_dropoff_positions with
    | [] -> 0
    | Some _ -> state.const.dropoff_cost 
  in
  *)
  let extra_cost2 = state.persist.constructed_this_turn * state.const.dropoff_cost in
  let cost = state.const.new_entity_energy_cost + extra_cost + extra_cost2 in
    state.energy.(state.my_id) >= cost
;;

let count_unfriendly_within state p_ship radius =
  List.fold_left (fun acc o_ship ->
    if o_ship.entity.owner = p_ship.entity.owner then acc
    else (
      let distance = Game.calculate_distance state p_ship.entity.position o_ship.entity.position in
      if distance <= radius then acc + 1 else acc
    )
  ) 0 state.persist.p_ships
;;

let update_inspiration_relevant state =
  List.iter (fun p_ship ->
    p_ship.inspiration_relevant <- (List.length p_ship.could_inspire) > 0
  ) state.persist.p_ships
(*
  List.iter (fun p_ship ->
    p_ship.inspiration_relevant <- (count_unfriendly_within state p_ship (state.const.inspiration_radius + 2) > 1)
  ) state.persist.p_ships
*)
;;

let update_could_inspire state =
  List.iter (fun p_ship ->
    p_ship.could_inspire <- List.filter (fun o_ship ->
      o_ship.entity.owner != p_ship.entity.owner
      && (Game.calculate_distance state p_ship.entity.position o_ship.entity.position <= state.const.inspiration_radius + 2)
    ) state.persist.p_ships
  ) state.persist.p_ships
;;

let create_dropoff_searches state =

  (*Debug.debug "All dropoff searches:\n";*)
  state.persist.all_dropoff_searches <- Iguana.time_fn Iguana.all_dropoff_searches state;
  state.persist.dropoff_search <- state.persist.all_dropoff_searches.(state.my_id); (* FIXME redundant now *)
  let weighted_dropoff_search = Iguana.weight_halite_low_good state state.map state.persist.dropoff_search in
  state.persist.weighted_dropoff_search <- weighted_dropoff_search;
  state.persist.safe_dropoff_search <- Iguana.safe_dropoff_search state true;
  (*Iguana.debug_int_grid state.persist.dropoff_search;*)
  state.persist.return_search <- Iguana.return_search state;


;;

let update_in_danger state =
  List.iter (fun p_ship ->
    let r, c = p_ship.entity.position in
    p_ship.in_danger <-
      state.persist.danger_maps.(p_ship.entity.owner).(r).(c) < 3
  ) state.persist.p_ships
;;

let find_better_tiles_nearby state p_ship =
  let better = ref [] in
  let dist = Array.map (Array.map (fun _ -> max_int)) state.map in
  let pr, pc = p_ship.entity.position in
  let value = state.map.(pr).(pc) in
  let should_terminate (row, col) =
    dist.(row).(col) > state.const.inspiration_radius
  in
  let visit = fun (row, col) next q -> 
    Search.simple_bfs_visit dist (row, col) next q;
    if (state.map.(row).(col) >= value) then (
      better := (row, col) :: !better
    )
  in
  let get_next = Search.grid_get_next dist in
  let init = (Iguana.dist_init dist [p_ship.entity.position]) in
  Search.short_bfs init visit get_next should_terminate;
  !better
;;

let print_contested state =
  List.iter (fun p_ship ->
    match p_ship.contested_target with
    | None -> ()
    | Some (row, col) ->
      let pr, pc = p_ship.entity.position in
      Debug.debug (Printf.sprintf "Ship %d at %d, %d wants to contest %d, %d\n" p_ship.entity.id pr pc row col);
  ) (Game.my_p_ships state)
;;

let update_contested state =
  List.iter (fun p_ship ->
    let better_tiles_nearby = find_better_tiles_nearby state p_ship in
    let contested = List.filter (fun (row, col) ->
      let dist = Game.calculate_distance state (row, col) p_ship.entity.position in
      let prow, pcol = p_ship.entity.position in
      let between_us = (state.persist.all_enemy_search.(prow).(pcol)) > (state.persist.all_enemy_search.(row).(col)) in (* distance from enemy to me should be greater than distance from enemy to target ... maybe FIXME check this *)
        between_us && (dist > 0)
        && (dist < max_int) 
        && (state.persist.all_enemy_search.(row).(col) >= dist)
        && (state.persist.all_enemy_search.(row).(col) <= state.const.inspiration_radius + 1)
    ) better_tiles_nearby in
    p_ship.contested <- contested
  ) (Game.my_p_ships state)
;;

let begin_turn state =

  (*Debug.debug ("game seed: " ^ (string_of_int state.const.game_seed) ^ "\n");*)
  Gc.full_major();
  state.persist.constructed_this_turn <- 0;
  state.persist.obstacles_updated <- false;
  state.persist.save_for_dropoff <- false;
  Iguana.update_condensed_areas_under_threat state;
  Iguana.update_cells_under_threat state;
  create_dropoff_searches state;
  Game.update_p_ships state;
  update_could_inspire state;
  update_inspiration_relevant state;
  Iguana.clear_intent_map state;
  Iguana.clear_desire_map state;
  Iguana.update_danger_maps state;
  update_in_danger state;
  update_contested state;

;;

let create_area_searches state =

  (*let dist = distances_to_good_areas_v2 state in*)
  (*Debug.debug "Area searches:\n";*)

  Iguana.time_fn Iguana.update_area_searches state;

  (*let condensed_map, searches = Iguana.time_fn Iguana.area_searches state in*)
  (*state.persist.condensed_map <- condensed_map;*)
  (*state.persist.searches <- searches;*)

;;

let balance_search state =

  let dropoff_positions_owned = List.map (fun base ->
    base.position, base.owner
  ) (state.shipyards @ state.dropoffs) in
  let balance_dist, balance = Iguana.voronoi_search state dropoff_positions_owned in
  Iguana.debug_int_grid balance;
  balance

;;

let update_crash_search state =
  state.persist.crash_search <- Iguana.create_crash_search state false
;;

let update_all_enemy_search state =
  state.persist.all_enemy_search <- Iguana.create_all_enemy_search state
;;

let finalise_ship_commands state =

  (*let time_before_finalise = Game.time_seconds_elapsed_this_turn state in*)
  let ship_commands = Iguana.finalise_moves_ships_first state in
  (*Debug.debug (Printf.sprintf "Found %d orders for %d ships\n" (List.length ship_commands) (List.length (Game.my_p_ships state)));*)
  (*let time_after_finalise = Game.time_seconds_elapsed_this_turn state in*)
  (*Debug.debug (Printf.sprintf "milliseconds elapsed during finalise_moves = %f\n" ((time_after_finalise -. time_before_finalise) *. 1000.));*)
  (*Debug.debug (Printf.sprintf "Number of ship commands = %d\n" (List.length ship_commands));*)
  ship_commands

;;

let finalise_other_commands state =

  try
    if 
    (* (Game.my_halite state >= state.const.new_entity_energy_cost) *)
    enough_halite_to_afford_ship state
    && (Iguana.should_build_more_ships state)
    && (Iguana.shipyard_unoccupied state state.my_id) then
      [Generate]
    else (
      []
    )
  with e -> (Debug.debug ((Printexc.to_string e) ^ "\n"); [])

;;

let endgame_stupidity state =
  let good_value = List.nth (state.persist.percentile_thresholds) 2 in
  if good_value < 200 then (
    1. -. ((float_of_int good_value) /. 200.)
  )
  else (
    0.
  )
;;

let update_stupidity state =
  match state.num_players with
  | 2 ->
    state.persist.stupidity <- max 0.5 (endgame_stupidity state)
  | _ ->
    state.persist.stupidity <- max 0.1 (endgame_stupidity state)
;;

let wants_to_stay_still state p_ship =
  let greatest_desire = List.fold_left (fun acc_desire desire ->
    match acc_desire with
    | None -> Some desire
    | Some best -> 
      if best.weight > desire.weight then acc_desire 
      else Some desire
  ) (None) p_ship.desired_moves in
  match greatest_desire with
  | Some desire -> 
    begin match desire.command with
      | Move (_, Still) -> true
      | _ -> false
    end
  | _ -> false
;;

let update_friendly_obstacles state =
  List.iter (fun p_ship ->
    if p_ship.entity.owner = state.my_id then (
      if wants_to_stay_still state p_ship then
        let row, col = p_ship.entity.position in
        state.persist.obstacles.(row).(col) <- true;
    )
  ) state.persist.p_ships;
  state.persist.obstacles_updated <- true;
;;

let update_enemy_obstacles state =
  List.iter (fun p_ship ->
    if p_ship.entity.owner != state.my_id then (
      let pos = p_ship.entity.position in
      let positions = pos :: Search.get_adjacent state.persist.obstacles pos in
      List.iter (fun (row, col) ->
        state.persist.obstacles.(row).(col) <- true;
      ) positions;
    )
  ) state.persist.p_ships;
;;

let clear_obstacles state =
  state.persist.obstacles <- Array.map (Array.map (fun _ -> false)) state.map;
;;

