open Type;;

let new_magic = function 
  | None ->
  {
    advisable_dropoff_quality_ratio = Fraction (15, 10);
    advisable_dropoff_continuation_ratio = Fraction(14, 10);
    ship_build_payoff_mul = Fraction (14, 10);
    dangerous_load_mul = Integer (16);
    enemy_dropoff_exclusion_radius = 6;
    friendly_dropoff_exclusion_radius = 8;
    endgame_high_halite = 450;
    crash_warn_downplay = 0.2;
    min_ships_before_dropoff = 11;
  }
  | Some state ->
    let adqr, adcr, sbpm = 
      if state.num_players = 4 then
      (
        Fraction (16, 10),
        Fraction(14, 10),
        Fraction(14, 10)
      )
      else (
        let reduction = 0 (* state.width / 8 - 4 *) in
        Fraction (16 - reduction, 10),
        Fraction(14 - reduction, 10),
        Fraction(12, 10)
      )
    in
    let radius_reduction = 5 - (state.width / 8 - 5) in
    {
      advisable_dropoff_quality_ratio = adqr;
      advisable_dropoff_continuation_ratio = adcr;
      ship_build_payoff_mul = sbpm;
      dangerous_load_mul = Integer (16);
      enemy_dropoff_exclusion_radius = 12 - radius_reduction;
      friendly_dropoff_exclusion_radius = 13 - radius_reduction;
      endgame_high_halite = 450;
      crash_warn_downplay = 0.25;
      min_ships_before_dropoff = 11;
    }
;;

let mul_int i magic =
  match magic with
  | Fraction (num, denom) ->
    i * num / denom
  | Integer num ->
    i * num
  | Float f ->
    int_of_float ((float_of_int i) *. f)
;;


