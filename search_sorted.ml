open Type;;

let turn = ref 0;;

let score_map_bonus grid p_ship (row, col) =
  float_of_int grid.(row).(col) /. 100.
;;

let score_medium_dest_bonus grid (row, col) =
  float_of_int grid.(row).(col)
;;

(* FIXME profit will also be calculated in the course of working out how many
 * turns to mine for *)
let mining_profit state grid turns_total (row, col) =
  let rec sub_profit halite turns =
    if turns > 0 then
      let profit = halite / state.const.extract_ratio in (* FIXME check rounding *)
      let next_halite = halite - profit in
      profit + sub_profit next_halite (turns - 1)
    else 0
  in
  let start_halite = grid.(row).(col) in
  let sub = sub_profit start_halite turns_total in
  let next_halite = start_halite - sub in
  let cost = (next_halite / state.const.move_cost_ratio) in
    Debug.debug (Printf.sprintf "With start halite of %d mining for %d turns the sub_profit is %d and the cost is %d for a mining_profit of %d leaving %d\n" start_halite turns_total sub cost (sub - cost) next_halite);
    sub - cost
;;

let limited_mining_turns state start_halite max_halite max_turns =
  let rec optimal halite turns total =
    if (total >= max_halite) || (turns >= max_turns) then (
      turns
    )
    else (
      let profit = halite / state.const.extract_ratio in
      let next_halite = halite - profit in
      optimal next_halite (turns + 1) (total + profit)
    )
  in
  optimal start_halite 0 0
;;

let mining_turns_at state p_ship grid (row, col) =
  let halite = grid.(row).(col) in
  let move_now_cost = halite / state.const.move_cost_ratio in
  let half_threshold = (List.nth state.persist.percentile_thresholds 3) in
  let max_turns = 
    if halite >= half_threshold then 5 (* FIXME revisit with better formula *)
    else if halite >= half_threshold / 2 then 2
    else if p_ship.entity.halite < move_now_cost || (halite >= half_threshold / 4) then 1
    else 0
  in
  Debug.debug (Printf.sprintf "max_turns = %d\n" max_turns);
  let can_collect = state.const.max_energy - p_ship.entity.halite in
  limited_mining_turns state halite can_collect max_turns
;;

let score_collect state map_grid medium_dest_grid p_ship prev_score next_score =
  let row, col = next_score.cell_row, next_score.cell_col in
  let map_bonus = score_map_bonus map_grid p_ship (row, col) in
  let dest_bonus = score_medium_dest_bonus medium_dest_grid (row, col) in
  let turns_here = mining_turns_at state p_ship map_grid (row, col) in
  Debug.debug (Printf.sprintf "turns_here = %d\n" turns_here);
  let profit_here = mining_profit state map_grid turns_here (row, col) in
  let turns_total = turns_here + prev_score.turns_total in
  let profit_total = profit_here + prev_score.profit_total in
  let score = (3. +. map_bonus +. dest_bonus) 
    *. (float_of_int profit_total) /. (float_of_int turns_total) 
  in
  Debug.debug (Printf.sprintf "Profit total = %d\n" profit_total);
  {
    visited = true;
    score = score;
    turns_here = turns_here;
    turns_total = turns_total;
    profit_here = profit_here;
    profit_total = profit_total;
    cell_row = row;
    cell_col = col;
    predecessor = Some prev_score;
  }
;;

let undirected_score_collect state map_grid p_ship prev_score next_score =
  let row, col = next_score.cell_row, next_score.cell_col in
  let map_bonus = score_map_bonus map_grid p_ship (row, col) in
  let turns_here = mining_turns_at state p_ship map_grid (row, col) in
  Debug.debug (Printf.sprintf "turns_here = %d\n" turns_here);
  let profit_here = mining_profit state map_grid turns_here (row, col) in
  let turns_total = turns_here + prev_score.turns_total in
  let profit_total = profit_here + prev_score.profit_total in
  let score = (3. +. map_bonus) 
    *. (float_of_int profit_total) /. (float_of_int turns_total) 
  in
  Debug.debug (Printf.sprintf "Profit total = %d\n" profit_total);
  {
    visited = true;
    score = score;
    turns_here = turns_here;
    turns_total = turns_total;
    profit_here = profit_here;
    profit_total = profit_total;
    cell_row = row;
    cell_col = col;
    predecessor = Some prev_score;
  }
;;

let empty_search_cell row col =
  {
    visited = false;
    score = 0.;
    turns_here = 0;
    turns_total = 0;
    profit_here = 0;
    profit_total = 0;
    cell_row = row;
    cell_col = col;
    predecessor = None;
  }
;;

let general_visit results_grid node next q score_f =
  let p_score = node in
  let to_add = List.fold_left (fun acc cell ->
    (*let cell = results_grid.(n_row).(n_col) in*)
    if not cell.visited then (
(*      Debug.debug (Printf.sprintf "Adding unvisited cell at %d, %d to queue of length %d\n" cell.cell_row cell.cell_col (List.length !q));*)
      let scored = score_f p_score cell in
      results_grid.(cell.cell_row).(cell.cell_col) <- scored;
      scored :: acc;
    )
    else acc
  ) [] next in
  let compare_score a b = 
    let presult = b.score -. a.score in
    if presult < 0. then -1 else if presult > 0. then 1 else 0
  in
  q := List.merge compare_score !q to_add;
  (*
  if !turn = 2 then (
      let grid = (Array.map (Array.map (fun cell -> if cell.visited then 129 else 0 (*cell.profit_total*))) results_grid) in
      Iguana.save_grid_picture "search_grid" grid 128;
  )
  *)
;;

let init_ship_search results_grid score l =
  List.iter (fun (row, col) ->
    results_grid.(row).(col) <- {
      results_grid.(row).(col) with
        score = score;
        visited = true;
    }
  ) l;
  ref (List.map (fun (row, col) ->
    results_grid.(row).(col)
  ) l)
;;

let get_adjacent grid loc =
  let size = (Array.length grid, Array.length grid.(0)) in
  List.map (fun offset -> 
    let (row, col) =
      Game.wrap_grid_position size (Game.add_position loc offset)
    in
      grid.(row).(col)
  ) Game.list_move_offsets
;;

let search init locs visit get_next score_f terminal =
  let final = ref None in
  let q = init locs in
  let finished = ref false in
  while not !finished do
    (*Debug.debug "s";*)
    match !q with
    | node :: tail ->
      q := tail;
      let next = get_next node in
      visit node next q score_f;
      if terminal node then (
        finished := true;
        final := Some node;
      )
    | [] -> finished := true
  done;
  !final
;;

let get_next_future_search future (row, col) =
  
;;

let get_next_unvisited grid cell =
  List.filter (fun cell ->
    not grid.(cell.cell_row).(cell.cell_col).visited
  ) (get_adjacent grid (cell.cell_row, cell.cell_col))
;;

let rec squares_to_collect target per_square =
  let next = target - per_square / 5 in
  if next < 0 then 1
  else 1 + squares_to_collect next per_square
;;

(*
let score_simpler state map p_ship prev_score next_score =
  let to_collect = state.const.max_energy - p_ship.entity.halite in
  {
    visited = true;
    score = next_score;
    turns_here = 1 (*turns_here*);
    turns_total = turns_total;
    profit_here = profit_here;
    profit_total = profit_total;
    cell_row = row;
    cell_col = col;
    predecessor = Some prev_score;
  }
;;
*)

(*
let score_collect2 state map_grid medium_dest_grid p_ship prev_score next_score =
  let row, col = next_score.cell_row, next_score.cell_col in
  let map_bonus = score_map_bonus map_grid p_ship (row, col) in
  let dest_bonus = score_medium_dest_bonus medium_dest_grid (row, col) in
  let turns_here = mining_turns_at state p_ship map_grid (row, col) in
  let profit_here = mining_profit state map_grid turns_here (row, col) in
  let turns_total = turns_here + prev_score.turns_total in
  let profit_total = profit_here + prev_score.profit_total in
  let score = (3. +. map_bonus +. dest_bonus) 
    *. (float_of_int profit_total) /. (float_of_int turns_total) 
  in
  {
    visited = true;
    score = score;
    turns_here = turns_here;
    turns_total = turns_total;
    profit_here = profit_here;
    profit_total = profit_total;
    cell_row = row;
    cell_col = col;
    predecessor = Some prev_score;
  }
;;
*)

let empty_search_cell row col =
  {
    visited = false;
    score = 0.;
    turns_here = 0;
    turns_total = 0;
    profit_here = 0;
    profit_total = 0;
    cell_row = row;
    cell_col = col;
    predecessor = None;
  }
;;

let general_visit results_grid node next q score_f =
  let p_score = node in
  let to_add = List.fold_left (fun acc cell ->
    (*let cell = results_grid.(n_row).(n_col) in*)
    if not cell.visited then (
(*      Debug.debug (Printf.sprintf "Adding unvisited cell at %d, %d to queue of length %d\n" cell.cell_row cell.cell_col (List.length !q));*)
      let scored = score_f p_score cell in
      results_grid.(cell.cell_row).(cell.cell_col) <- scored;
      scored :: acc;
    )
    else acc
  ) [] next in
  let compare_score a b = 
    let presult = b.score -. a.score in
    if presult < 0. then -1 else if presult > 0. then 1 else 0
  in
  q := List.merge compare_score !q to_add;
  if !turn = 2 then (
      let grid = (Array.map (Array.map (fun cell -> if cell.visited then 129 else 0 (*cell.profit_total*))) results_grid) in
      Iguana.save_grid_picture "search_grid" grid 128;
  )
;;

let init_ship_search results_grid score l =
  List.iter (fun (row, col) ->
    results_grid.(row).(col) <- {
      results_grid.(row).(col) with
        score = score;
        visited = true;
    }
  ) l;
  ref (List.map (fun (row, col) ->
    results_grid.(row).(col)
  ) l)
;;

let get_adjacent grid loc =
  let size = (Array.length grid, Array.length grid.(0)) in
  List.map (fun offset -> 
    let (row, col) =
      Game.wrap_grid_position size (Game.add_position loc offset)
    in
      grid.(row).(col)
  ) Game.list_move_offsets
;;

let search init locs visit get_next score_f terminal =
  let final = ref None in
  let q = init locs in
  let finished = ref false in
  while not !finished do
    (*Debug.debug "s";*)
    match !q with
    | node :: tail ->
      q := tail;
      let next = get_next node in
      visit node next q score_f;
      if terminal node then (
        finished := true;
        final := Some node;
      )
    | [] -> finished := true
  done;
  !final
;;

let get_next_unvisited grid cell =
  List.filter (fun cell ->
    not grid.(cell.cell_row).(cell.cell_col).visited
  ) (get_adjacent grid (cell.cell_row, cell.cell_col))
;;

(*
let score_simpler state map p_ship prev_score next_score =
  
;;
*)

let ship_search state dest_grid p_ship =
  turn := state.turn;
  Debug.debug (Printf.sprintf "Mining ship halite = %d\n" p_ship.entity.halite);
  let results_grid = Array.mapi (fun ir row -> Array.mapi (fun ic _ -> empty_search_cell ir ic) row) state.map in
  let init = init_ship_search results_grid 0. in
  let visit = general_visit results_grid in
  let get_next = get_next_unvisited results_grid in
(*let score_collect state map_grid medium_dest_grid p_ship prev_score (row, col) =*)
(*
  let medium_dest_grid = match p_ship.medium_destination with 
    | None -> None
    | Some (row, col) -> 
*)
  let score_f = score_collect state state.map dest_grid p_ship in
  (*let terminal next = false in*)
  (*let score_f = fun p_score n_score -> {n_score with visited = true; score = p_score.score +. 1.} in*)
  let terminal node = node.profit_total > state.const.max_energy * 8 / 10 in
  let final_position = search init [p_ship.entity.position] visit get_next score_f terminal in
  results_grid, final_position
;;


let undirected_ship_search state p_ship =
  turn := state.turn;
  Debug.debug (Printf.sprintf "Mining ship halite = %d\n" p_ship.entity.halite);
  let results_grid = Array.mapi (fun ir row -> Array.mapi (fun ic _ -> empty_search_cell ir ic) row) state.map in
  let init = init_ship_search results_grid 0. in
  let visit = general_visit results_grid in
  let get_next = get_next_unvisited results_grid in
(*let score_collect state map_grid medium_dest_grid p_ship prev_score (row, col) =*)
(*
  let medium_dest_grid = match p_ship.medium_destination with 
    | None -> None
    | Some (row, col) -> 
*)
  let score_f = undirected_score_collect state state.map p_ship in
  (*let terminal next = false in*)
  (*let score_f = fun p_score n_score -> {n_score with visited = true; score = p_score.score +. 1.} in*)
  let terminal node = node.profit_total > state.const.max_energy * 8 / 10 in
  let final_position = search init [p_ship.entity.position] visit get_next score_f terminal in
  results_grid, final_position
;;

let ship_search state dest_grid p_ship =
  turn := state.turn;
  Debug.debug (Printf.sprintf "Mining ship halite = %d\n" p_ship.entity.halite);
  let results_grid = Array.mapi (fun ir row -> Array.mapi (fun ic _ -> empty_search_cell ir ic) row) state.map in
  let init = init_ship_search results_grid 0. in
  let visit = general_visit results_grid in
  let get_next = get_next_unvisited results_grid in
(*let score_collect state map_grid medium_dest_grid p_ship prev_score (row, col) =*)
(*
  let medium_dest_grid = match p_ship.medium_destination with 
    | None -> None
    | Some (row, col) -> 
*)
  let score_f = score_collect state state.map dest_grid p_ship in
  (*let terminal next = false in*)
  (*let score_f = fun p_score n_score -> {n_score with visited = true; score = p_score.score +. 1.} in*)
  let terminal node = node.profit_total > state.const.max_energy * 8 / 10 in
  let final_position = search init [p_ship.entity.position] visit get_next score_f terminal in
  results_grid, final_position
;;


